//
//  ZCLoadingModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCLoadingModel.h"


@implementation ZCLoadingInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end

@implementation ZCLoadingConditionModel

@end

@implementation ZCLoadingModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCLoadingInfoModel class]};
}

@end
