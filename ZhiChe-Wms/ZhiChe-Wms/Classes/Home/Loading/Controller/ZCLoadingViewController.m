//
//  ZCLoadingViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCLoadingViewController.h"
#import "ZCPutInStorageTableViewCell.h"
#import "UWRQViewController.h"
#import "ZCLoadingModel.h"
#import "ZCLoadingDetailViewController.h"

static NSString *loadingCellID = @"ZCPutInStorageTableViewCell";

@interface ZCLoadingViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate, uwRQDelegate>

@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UITableViewController *tableViewC;
@property (nonatomic, strong) UIButton *scanButton;
@property (nonatomic, strong) UISearchController *searchC;
@property (nonatomic, strong) NSMutableArray *listArray;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) ZCLoadingModel *loadingModel;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ZCLoadingViewController
#pragma mark - 懒加载
- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, 40)];
        _searchView.backgroundColor = [UIColor whiteColor];
    }
    return _searchView;
}

- (UITableViewController *)tableViewC {
    if (!_tableViewC) {
        _tableViewC = [[UITableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        _tableViewC.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableViewC.tableView.showsVerticalScrollIndicator = NO;
        _tableViewC.tableView.showsHorizontalScrollIndicator = NO;
        _tableViewC.tableView.delegate = self;
        _tableViewC.tableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            _tableViewC.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableViewC.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableViewC.tableView.scrollIndicatorInsets = _tableViewC.tableView.contentInset;
            _tableViewC.tableView.estimatedRowHeight = 0;
            _tableViewC.tableView.estimatedSectionFooterHeight = 0;
            _tableViewC.tableView.estimatedSectionHeaderHeight = 0;
        }
        [_tableViewC.tableView registerClass:[ZCPutInStorageTableViewCell class] forCellReuseIdentifier:loadingCellID];
        _tableViewC.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        _tableViewC.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }
    return _tableViewC;
}

- (UIButton *)scanButton {
    if (!_scanButton) {
        _scanButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [_scanButton setImage:[UIImage imageNamed:@"scanQR"] forState:UIControlStateNormal];
        [_scanButton addTarget:self action:@selector(scanButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scanButton;
}

- (UISearchController *)searchC {
    if (!_searchC) {
        _searchC = [[UISearchController alloc] initWithSearchResultsController:self.tableViewC];
        _searchC.delegate = self;
        _searchC.searchResultsUpdater = self;
        _searchC.searchBar.delegate = self;
        _searchC.searchBar.placeholder = @"请输入订单号/车架号";
        [_searchC.searchBar addSubview:self.scanButton];
        __weak typeof(self)weakSelf = self;
        [self.scanButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.searchC.searchBar.mas_centerY);
            make.right.equalTo(weakSelf.searchC.searchBar.mas_right).offset(-15);
        }];
    }
    return _searchC;
}

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"装车交验";
    self.tabBarController.tabBar.hidden = YES;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.definesPresentationContext = YES;
    [self.view addSubview:self.searchView];
    [self.searchView addSubview:self.searchC.searchBar];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.searchC.searchBar.text.length > 0) {
        [self loadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = nil;
}

#pragma mark - 网络请求
- (void)loadData {
    self.currentPage = 1;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.searchC.searchBar.text forKey:@"condition[key]"];
    [param setObject:@(self.currentPage) forKey:@"current"];
    [param setObject:@(10) forKey:@"size"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:LoadingList params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                [weakSelf.listArray removeAllObjects];
                weakSelf.loadingModel = [ZCLoadingModel yy_modelWithDictionary:responseObject[@"data"]];
                [weakSelf.listArray addObjectsFromArray:weakSelf.loadingModel.records];
                [weakSelf.tableViewC.tableView reloadData];
                [weakSelf.tableViewC.tableView.mj_header endRefreshing];
                if (weakSelf.loadingModel.records.count < 10) {
                    [weakSelf.tableViewC.tableView.mj_footer endRefreshingWithNoMoreData];
                }else {
                    [weakSelf.tableViewC.tableView.mj_footer resetNoMoreData];
                }
            }else {
                [weakSelf.tableViewC.tableView.mj_header endRefreshing];
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            [weakSelf.tableViewC.tableView.mj_header endRefreshing];
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

- (void)loadMoreData {
    self.currentPage++;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.searchC.searchBar.text forKey:@"condition[key]"];
    [param setObject:@(self.currentPage) forKey:@"current"];
    [param setObject:@(10) forKey:@"size"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    if (self.currentPage <= [self.loadingModel.pages integerValue]) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [ZCHttpTool postWithURL:LoadingList params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([responseObject[@"code"] integerValue] == 0) {
                    [weakSelf.hud hideAnimated:YES];
                    weakSelf.loadingModel = [ZCLoadingModel yy_modelWithDictionary:responseObject[@"data"]];
                    [weakSelf.listArray addObjectsFromArray:weakSelf.loadingModel.records];
                    [weakSelf.tableViewC.tableView reloadData];
                    if (weakSelf.loadingModel.records.count < 10) {
                        [weakSelf.tableViewC.tableView.mj_footer endRefreshingWithNoMoreData];
                    }else {
                        [weakSelf.tableViewC.tableView.mj_footer resetNoMoreData];
                    }
                }else {
                    [weakSelf.tableViewC.tableView.mj_footer endRefreshing];
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = responseObject[@"message"];
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                [weakSelf.tableViewC.tableView.mj_footer endRefreshing];
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = error.localizedDescription;
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }];
        });
    }else {
        [self.tableViewC.tableView.mj_footer endRefreshing];
    }
}

#pragma mark - 按钮点击
- (void)backButtonClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scanButtonClick:(UIButton *)sender {
    UWRQViewController *rqVC = [[UWRQViewController alloc] init];
    rqVC.delegate = self;
    [self.navigationController pushViewController:rqVC animated:YES];
}

#pragma mark - uwRQDelegate
- (void)uwRQFinshedScan:(NSString *)result {
    NSArray *arr = [result componentsSeparatedByString:@","];
    NSString *keyId = [arr firstObject];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:keyId forKey:@"condition[key]"];
    [param setObject:@"SCAN" forKey:@"condition[visitType]"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:LoadingDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                if ([responseObject[@"data"] isEqual:[NSNull null]]) {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = @"未查到相关数据";
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }else {
                    [weakSelf.hud hideAnimated:YES];
                    ZCLoadingDetailViewController *detailVC = [[ZCLoadingDetailViewController alloc] init];
                    detailVC.keyId = keyId;
                    detailVC.visitType = @"SCAN";
                    [weakSelf.navigationController pushViewController:detailVC animated:YES];
                }
                
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
   
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableViewC.view addSubview:self.hud];
    [self loadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.listArray removeAllObjects];
    [self.tableViewC.tableView reloadData];
}

#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController {
    self.scanButton.hidden = YES;
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    self.scanButton.hidden = NO;
}

#pragma mark - UISearchResultUpdating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.isActive) {
        searchController.searchBar.showsCancelButton = YES;
        UIButton *cancelButton = [searchController.searchBar valueForKey:@"cancelButton"];
        if (cancelButton) {
            [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
            [cancelButton setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPutInStorageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:loadingCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.typeTitle = @"车型";
    cell.statusTitle = @"状态";
    ZCLoadingInfoModel *infoModel = self.listArray[indexPath.section];
    cell.loadingInfoModel = infoModel;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return space(360);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return space(20);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCLoadingDetailViewController *detailVC = [[ZCLoadingDetailViewController alloc] init];
    ZCLoadingInfoModel *infoModel = self.listArray[indexPath.section];
    detailVC.keyId = infoModel.Id;
    detailVC.visitType = @"CLICK";
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
