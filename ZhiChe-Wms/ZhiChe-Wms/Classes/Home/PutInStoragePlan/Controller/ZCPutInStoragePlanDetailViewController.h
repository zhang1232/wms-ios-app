//
//  ZCPutInStoragePlanDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCPutInStoragePlanDetailViewController : UIViewController
@property (nonatomic, copy) NSString *houseId;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, assign) BOOL isClick;
@property (nonatomic, assign) BOOL fromPlan;

@end
