//
//  ZCTaskModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCTaskModel : NSObject

@property (nonatomic, copy) NSString *token;          //用户token
@property (nonatomic, copy) NSString *whCode;         //仓库code
@property (nonatomic, copy) NSString *userCode;       //用户编码
@property (nonatomic, copy) NSString *originCode;     //起运地编码
@property (nonatomic, copy) NSString *originName;     //起运地名称
@property (nonatomic, copy) NSString *destCode;       //目的地编码
@property (nonatomic, copy) NSString *destName;       //目的地名称
@property (nonatomic, copy) NSString *vin;            //VIN码(车架号)
@property (nonatomic, copy) NSString *orderNo;        //订单号
@property (nonatomic, copy) NSString *wayBillNo;      //运单号
@property (nonatomic, copy) NSString *taskType;       //任务类型(10:寻车，20：移车，30：提车)
@property (nonatomic, copy) NSString *taskStatus;     //任务状态(10：创建，20：开始，30：完成，50：取消)
@property (nonatomic, copy) NSString *orderReleaseGid;   //系统单号
@property (nonatomic, copy) NSString *dispatchNo;     //指令号
@property (nonatomic, copy) NSString *taskId;         //任务Id
@property (nonatomic, copy) NSString *vehicle;        //车型描述
@property (nonatomic, copy) NSString *stanVehicleType;    //标准车型
@property (nonatomic, copy) NSString *otmStatus;
@property (nonatomic, copy) NSString *driverCode;     //司机编号
@property (nonatomic, copy) NSString *driverName;     //司机姓名
@property (nonatomic, copy) NSString *driverPhone;    //司机电话
@property (nonatomic, copy) NSString *startTime;      //任务开始时间
@property (nonatomic, copy) NSString *finishTime;     //任务完成时间
@property (nonatomic, copy) NSString *releaseId;      //指令Id
@property (nonatomic, copy) NSString *qrCode;         //二维码
@property (nonatomic, copy) NSString *serviceProviderName;     //供应方

@end

@interface ZCTaskPageInfoModel : NSObject
@property (nonatomic, copy) NSString *pageNo;          //页码
@property (nonatomic, copy) NSString *pageSize;        //条数
@property (nonatomic, copy) NSString *totalRecord;     //总记录
@property (nonatomic, copy) NSString *totalPage;       //总页数
@property (nonatomic, copy) NSString *order;
@property (nonatomic, copy) NSString *startIndex;

@end

@interface ZCTaskDetailModel : NSObject

@property (nonatomic, copy) NSString *token;              //用户token
@property (nonatomic, copy) NSString *whCode;             //仓库code
@property (nonatomic, copy) NSString *userCode;           //用户编码
@property (nonatomic, copy) NSString *originCode;         //起运地编码
@property (nonatomic, copy) NSString *originName;         //起运地名称
@property (nonatomic, copy) NSString *destCode;           //目的地编码
@property (nonatomic, copy) NSString *destName;           //目的地名称
@property (nonatomic, copy) NSString *vin;                //VIN码(车架号)
@property (nonatomic, copy) NSString *orderNo;            //订单号
@property (nonatomic, copy) NSString *wayBillNo;          //运单号
@property (nonatomic, copy) NSString *taskType;           //任务类型(10：寻车，20：移车，30：提车)
@property (nonatomic, copy) NSString *taskStatus;         //任务状态(10：创建，20：开始，30：完成，50：取消)
@property (nonatomic, copy) NSString *orderReleaseGid;     //系统单号
@property (nonatomic, copy) NSString *dispatchNo;          //指令号
@property (nonatomic, copy) NSString *taskId;              //任务Id
@property (nonatomic, copy) NSString *vehicle;             //车型描述
@property (nonatomic, copy) NSString *stanVehicleType;     //标准车型
@property (nonatomic, copy) NSString *otmStatus;
@property (nonatomic, copy) NSString *driverCode;          //司机编号
@property (nonatomic, copy) NSString *driverName;          //司机姓名
@property (nonatomic, copy) NSString *driverPhone;         //司机电话
@property (nonatomic, copy) NSString *startTime;           //任务开始时间
@property (nonatomic, copy) NSString *finishTime;          //任务完成时间
@property (nonatomic, copy) NSString *releaseId;           //运单Id
@property (nonatomic, copy) NSString *qrCode;              //二维码
@property (nonatomic, copy) NSString *taskCreate;          //任务创建时间
@property (nonatomic, copy) NSString *taskStart;           //任务开始时间
@property (nonatomic, copy) NSString *taskFinish;          //任务完成时间
@property (nonatomic, copy) NSString *taskNode;            //事件节点
@property (nonatomic, copy) NSString *bindTime;            //二维码绑定时间
@property (nonatomic, copy) NSString *bindId;
@property (nonatomic, copy) NSString *isCanSend;           //是否发运
@property (nonatomic, copy) NSString *pointId;             //发车点ID


@end


