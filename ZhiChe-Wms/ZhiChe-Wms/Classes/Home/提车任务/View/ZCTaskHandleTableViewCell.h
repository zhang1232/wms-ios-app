//
//  ZCTaskHandleTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/16.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCTaskModel.h"

@protocol ZCTaskHandleTableViewCellDelegate <NSObject>

- (void)taskHandleTableViewCellTaskButtonActionWithTaskDetailModel:(ZCTaskDetailModel *)detailModel;
- (void)taskHandleTableViewCellQRButtonActionWithTaskDetailModel:(ZCTaskDetailModel *)detailModel;
- (void)taskHandleTableViewCellAbnormalActionWithTaskDetailModel:(ZCTaskDetailModel *)detailModel;

@end

@interface ZCTaskHandleTableViewCell : UITableViewCell
@property (nonatomic, strong) ZCTaskDetailModel *taskDetailModel;

@property (nonatomic, weak) id<ZCTaskHandleTableViewCellDelegate> delegate;

@end
