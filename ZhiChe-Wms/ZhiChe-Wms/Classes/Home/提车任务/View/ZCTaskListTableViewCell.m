//
//  ZCTaskListTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTaskListTableViewCell.h"

@interface ZCTaskListTableViewCell ()
@property (nonatomic, strong) UILabel *orderTitleLabel;
@property (nonatomic, strong) UILabel *orderNumLabel;
@property (nonatomic, strong) UIImageView *startImageView;
@property (nonatomic, strong) UILabel *startLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UIImageView *endImageView;
@property (nonatomic, strong) UILabel *endLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *carTitleLabel;
@property (nonatomic, strong) UILabel *carNumLabel;

@end

@implementation ZCTaskListTableViewCell
#pragma mark - 懒加载
- (UILabel *)orderTitleLabel {
    if (!_orderTitleLabel) {
        _orderTitleLabel = [[UILabel alloc] init];
        _orderTitleLabel.text = @"订单号";
        _orderTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _orderTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderTitleLabel;
}

- (UILabel *)orderNumLabel {
    if (!_orderNumLabel) {
        _orderNumLabel = [[UILabel alloc] init];
        _orderNumLabel.text = @"20288837";
        _orderNumLabel.textColor = ZCColor(0x000000, 0.87);
        _orderNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderNumLabel;
}

- (UIImageView *)startImageView {
    if (!_startImageView) {
        _startImageView = [[UIImageView alloc] init];
        _startImageView.image = [UIImage imageNamed:@"from"];
    }
    return _startImageView;
}

- (UILabel *)startLabel {
    if (!_startLabel) {
        _startLabel = [[UILabel alloc] init];
        _startLabel.text = @"全顺库";
        _startLabel.textColor = ZCColor(0x000000, 0.54);
        _startLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _startLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.text = @"类型    提车任务";
        _typeLabel.textColor = ZCColor(0x000000, 0.34);
        _typeLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _typeLabel;
}

- (UIImageView *)endImageView {
    if (!_endImageView) {
        _endImageView = [[UIImageView alloc] init];
        _endImageView.image = [UIImage imageNamed:@"to"];
    }
    return _endImageView;
}

- (UILabel *)endLabel {
    if (!_endLabel) {
        _endLabel = [[UILabel alloc] init];
        _endLabel.text = @"电网库";
        _endLabel.textColor = ZCColor(0x000000, 0.54);
        _endLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _endLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"状态    创建";
        _statusLabel.textColor = ZCColor(0x000000, 0.34);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UILabel *)carTitleLabel {
    if (!_carTitleLabel) {
        _carTitleLabel = [[UILabel alloc] init];
        _carTitleLabel.text = @"车架号";
        _carTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _carTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carTitleLabel;
}

- (UILabel *)carNumLabel {
    if (!_carNumLabel) {
        _carNumLabel = [[UILabel alloc] init];
        _carNumLabel.text = @"33441";
        _carNumLabel.textColor = ZCColor(0x000000, 0.87);
        _carNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carNumLabel;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.orderTitleLabel];
        [self addSubview:self.orderNumLabel];
        [self addSubview:self.startImageView];
        [self addSubview:self.startLabel];
        [self addSubview:self.typeLabel];
        [self addSubview:self.endImageView];
        [self addSubview:self.endLabel];
        [self addSubview:self.statusLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.carTitleLabel];
        [self addSubview:self.carNumLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setTaskModel:(ZCTaskModel *)taskModel {
    _taskModel = taskModel;
    _orderNumLabel.text = taskModel.orderNo;
    _startLabel.text = taskModel.originName;
    _endLabel.text = taskModel.destName;
    if ([taskModel.taskType integerValue] == 10) {
        _typeLabel.text = @"类型    寻车任务";
    }else if ([taskModel.taskType integerValue] == 20) {
        _typeLabel.text = @"类型    移车任务";
    }else if ([taskModel.taskType integerValue] == 30) {
        _typeLabel.text = @"类型    提车任务";
    }
    if ([taskModel.taskStatus integerValue] == 10) {
        _statusLabel.text = @"状态    创建";
    }else if ([taskModel.taskStatus integerValue] == 20) {
        _statusLabel.text = @"状态    开始";
    }else if ([taskModel.taskStatus integerValue] == 30) {
        _statusLabel.text = @"状态    完成";
    }else if ([taskModel.taskStatus integerValue] == 50) {
        _statusLabel.text = @"状态    取消";
    }
    _carNumLabel.text = taskModel.vin;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_orderTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(40));
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.width.mas_equalTo(space(120));
    }];
    
    [_orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.orderTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderTitleLabel.mas_right).offset(space(20));
    }];
    
    [_startImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.top.equalTo(weakSelf.orderTitleLabel.mas_bottom).offset(space(40));
    }];
    
    [_startLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startImageView.mas_centerY);
        make.left.equalTo(weakSelf.startImageView.mas_right).offset(space(20));
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startLabel.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
    }];
    
    [_endImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.startImageView.mas_bottom).offset(space(30));
        make.centerX.equalTo(weakSelf.startImageView.mas_centerX);
    }];
    
    [_endLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endImageView.mas_centerY);
        make.left.equalTo(weakSelf.startLabel.mas_left);
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeLabel.mas_left);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.endImageView.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(2));
    }];
    
    [_carTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.lineView.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.orderTitleLabel.mas_left);
    }];
    
    [_carNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.carTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderNumLabel.mas_left);
    }];
}

@end
