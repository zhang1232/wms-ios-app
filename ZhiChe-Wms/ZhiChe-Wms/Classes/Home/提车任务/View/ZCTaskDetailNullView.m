//
//  ZCTaskDetailNullView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTaskDetailNullView.h"


@interface ZCTaskDetailNullView ()

@property (nonatomic, strong) UILabel *tipLabel;

@end

@implementation ZCTaskDetailNullView
#pragma mark - 懒加载
- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.text = @"暂无数据";
        _tipLabel.font = [UIFont boldSystemFontOfSize:FontSize(36)];
        _tipLabel.textColor = ZCColor(0x000000, 0.54);
    }
    return _tipLabel;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tipLabel];
    }
    return self;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
}


@end
