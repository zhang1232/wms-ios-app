//
//  ZCAbnormalModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/21.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalModel.h"

@implementation ZCAbnormalModel

@end

@implementation ZCAbnormalInfoModel

@end

@implementation ZCAbnormalListDetailModel

@end

@implementation ZCAbnormalListModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"listId" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"exceptionDetail" : [ZCAbnormalListDetailModel class]};
}
@end


@implementation ZCAbnormalSignModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"signId" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"exceptionDetails" : [ZCAbnormalListDetailModel class]};
}
@end

@implementation ZCAbnormalExceptionModel

@end

@implementation ZCAbnormalAllModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCAbnormalSignModel class]};
}

@end

@implementation ZCAbnormalMisssingPartModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"partId" : @"id"};
}

@end

@implementation ZCAbnormalMissingModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"classId" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"childComponent" : [ZCAbnormalMisssingPartModel class]};
}

@end

