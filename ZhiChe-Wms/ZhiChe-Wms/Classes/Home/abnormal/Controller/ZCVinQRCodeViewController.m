//
//  ZCVinQRCodeViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/23.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCVinQRCodeViewController.h"
#import "ZCCreateQRCode.h"

@interface ZCVinQRCodeViewController ()

@property (nonatomic, strong) UIImageView *qrCodeImageView;
@property (nonatomic, strong) UILabel *vinLabel;

@end

@implementation ZCVinQRCodeViewController
#pragma mark - 懒加载
- (UIImageView *)qrCodeImageView {
    if (!_qrCodeImageView) {
        _qrCodeImageView = [[UIImageView alloc] init];
    }
    return _qrCodeImageView;
}

- (UILabel *)vinLabel {
    if (!_vinLabel) {
        _vinLabel = [[UILabel alloc] init];
        _vinLabel.textColor = ZCColor(0xff8213, 1);
        _vinLabel.font = [UIFont systemFontOfSize:FontSize(32)];
        _vinLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _vinLabel;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = @"车架号二维码";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.qrCodeImageView];
    [self.view addSubview:self.vinLabel];
    [self updateViewConstraints];
    UIImage *qrImage = [ZCCreateQRCode createQRCodeFromString:self.vin codeSize:SCREENWIDTH-space(200)];
    self.qrCodeImageView.image = qrImage;
    self.vinLabel.text = self.vin;
}

#pragma mark - 按钮点击
- (void)backToHome {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 布局
- (void)updateViewConstraints {
    [super updateViewConstraints];
    __weak typeof(self)weakSelf = self;
    [_qrCodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view.mas_top).offset(space(300));
        make.left.equalTo(weakSelf.view.mas_left).offset(space(100));
        make.right.equalTo(weakSelf.view.mas_right).offset(-space(100));
        make.height.equalTo(weakSelf.qrCodeImageView.mas_width);
    }];
    
    [_vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.qrCodeImageView.mas_bottom).offset(space(60));
        make.left.equalTo(weakSelf.qrCodeImageView.mas_left);
        make.right.equalTo(weakSelf.qrCodeImageView.mas_right);
    }];
}


@end
