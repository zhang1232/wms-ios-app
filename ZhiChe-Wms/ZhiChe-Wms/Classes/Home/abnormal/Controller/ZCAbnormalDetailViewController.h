//
//  ZCAbnormalDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCAbnormalModel.h"

@interface ZCAbnormalDetailViewController : UIViewController

@property (nonatomic, strong) ZCAbnormalInfoModel *infoModel;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, assign) BOOL fromAbnormal;
@property (nonatomic, copy) NSString *taskType;

@end
