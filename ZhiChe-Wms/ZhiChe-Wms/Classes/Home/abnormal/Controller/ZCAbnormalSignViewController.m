//
//  ZCAbnormalSignViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignViewController.h"
#import "ZCTaskTotalNumTableViewCell.h"
#import "ZCAbnormalMissingTableViewCell.h"
#import "ZCAbnormalSignTableViewCell.h"
#import "ZCAbnormalDetailViewController.h"
#import "ZCAbnormalModel.h"
#import "ZCVinQRCodeViewController.h"
#import "ZCAbnormalAllViewController.h"
#import "ZCAbnormalLackViewController.h"
#import "ZCQualityTestingModel.h"
#import "ZCPutInStoragePlanModel.h"
#import "ZCAbnormalSendTableViewCell.h"

static NSString *orderInfoCell = @"ZCTaskTotalNumTableViewCell";
static NSString *missingCell = @"ZCAbnormalMissingTableViewCell";
static NSString *signCell = @"ZCAbnormalSignTableViewCell";
static NSString *sendCellID = @"ZCAbnormalSendTableViewCell";

@interface ZCAbnormalSignViewController ()<UITableViewDelegate, UITableViewDataSource, ZCAbnormalSignTableViewCellDelegate, ZCAbnormalSendTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCAbnormalModel *abnormalModel;
@property (nonatomic, strong) UIButton *finishButton;
@property (nonatomic, copy) NSString *exceptTotal;
@property (nonatomic, strong) NSArray *exceptInfoArray;
@property (nonatomic, strong) NSArray *cloakInfoArray;
@property (nonatomic, copy) NSString *missingPart;
@property (nonatomic, strong) ZCQualityTestingModel *testingModel;
@property (nonatomic, assign) NSInteger testingStatus;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, copy) NSString *sendOrNot;
@end

@implementation ZCAbnormalSignViewController
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT - space(150)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ZCColor(0xf7f8fb, 1);
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        
        [_tableView registerClass:[ZCTaskTotalNumTableViewCell class] forCellReuseIdentifier:orderInfoCell];
        [_tableView registerClass:[ZCAbnormalMissingTableViewCell class] forCellReuseIdentifier:missingCell];
        [_tableView registerClass:[ZCAbnormalSignTableViewCell class] forCellReuseIdentifier:signCell];
        [_tableView registerClass:[ZCAbnormalSendTableViewCell class] forCellReuseIdentifier:sendCellID];
    }
    return _tableView;
}

- (UIButton *)finishButton {
    if (!_finishButton) {
        _finishButton = [[UIButton alloc] initWithFrame:CGRectMake(space(80), SCREENHEIGHT - space(130), SCREENWIDTH - space(160), space(90))];
        [_finishButton setTitle:@"完成" forState:UIControlStateNormal];
        [_finishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_finishButton setBackgroundColor:ZCColor(0xff8213, 1)];
        _finishButton.layer.masksToBounds = YES;
        _finishButton.layer.cornerRadius = space(10);
        [_finishButton addTarget:self action:@selector(finishButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishButton;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = @"异常登记";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.finishButton];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30 || [self.taskType integerValue] == 60) {
        //指令、寻车、移车、提车任务、装车交验
        self.tableView.scrollEnabled = YES;
        if ([self.taskType integerValue] == 60) {
            if (self.orderNoAlloc.length > 0) {
                [self.hud showAnimated:YES];
                [self loadSignTotalData];
                [self loadSignData];
            }
        }else {
            [self loadTaskDetailData];
        }
    }else if ([self.taskType integerValue] == 40) {
        //收车质检
        self.testingStatus = 0;
        self.tableView.scrollEnabled = YES;
        [self.finishButton setTitle:@"验车登记" forState:UIControlStateNormal];
        [self loadQualityTestingDetailData];

    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 ||[self.taskType integerValue] == 50) {
        //分配入库、出库确认、备料任务
        self.tableView.scrollEnabled = YES;
        if (self.orderNoAlloc.length > 0) {
            [self.hud showAnimated:YES];
            [self loadSignTotalData];
            [self loadSignData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
}

#pragma mark - 属性设置
- (void)setIsCanSend:(NSString *)isCanSend {
    _isCanSend = isCanSend;
    self.sendOrNot = isCanSend;
}

#pragma mark - 按钮点击
- (void)backToHome {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)finishButtonClick {
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
        //指令、寻车、移车、提车任务
        [self sendRemarkWithVin:self.abnormalModel.vin];
    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60){
        //分配入库、出库确认、备料任务、装车交验
        [self sendRemarkWithVin:self.vinAlloc];
    }else if ([self.taskType integerValue] == 40) {
        //收车质检
        [self commitTesting];
    }
}

- (void)commitTesting {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"houseId"];
    [param setObject:self.testingModel.Id forKey:@"key"];
    [param setObject:self.testingStatus > 0 ? @(30) : @(10) forKey:@"status"];
    [param setObject:self.sendOrNot.length > 0 ? self.sendOrNot : @"" forKey:@"isCanSend"];
    

    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:QualityTestingCommit params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

- (void)sendRemarkWithVin:(NSString *)vin {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"condition[houseId]"];
    [param setObject:vin.length > 0 ? vin : @(0) forKey:@"condition[lotNo1]"];
    [param setObject:self.sendOrNot.length > 0 ? self.sendOrNot : @"" forKey:@"condition[isCanSend]"];
    
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:ExceptionFinish params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

#pragma mark - 网络请求
//异常任务详情
- (void)loadTaskDetailData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.releaseId forKey:@"id"];
    [param setObject:self.taskType forKey:@"taskType"];
    
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:TaskDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"success"] boolValue]) {
                weakSelf.abnormalModel = [ZCAbnormalModel yy_modelWithDictionary:responseObject[@"data"]];
                weakSelf.sendOrNot = weakSelf.abnormalModel.isCanSend;
                [weakSelf.tableView reloadData];
                if (weakSelf.abnormalModel.orderNo.length > 0) {
                    [weakSelf loadSignTotalData];
                    [weakSelf loadSignData];
                }else {
                    [weakSelf.hud hideAnimated:YES];
                }
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

//收车质检详情
- (void)loadQualityTestingDetailData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"houseId"];
    [param setObject:self.releaseId forKey:@"key"];
    [param setObject:self.taskType forKey:@"taskType"];
    [param setObject:self.visitType forKey:@"visitType"];

    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:QualityTestingDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                weakSelf.testingModel = [ZCQualityTestingModel yy_modelWithDictionary:responseObject[@"data"]];
                weakSelf.sendOrNot = weakSelf.testingModel.isCanSend;
                [weakSelf.tableView reloadData];
                if (weakSelf.testingModel.ownerOrderNo.length > 0) {
                    [weakSelf loadSignTotalData];
                    [weakSelf loadSignData];
                }else {
                    [weakSelf.hud hideAnimated:YES];
                }
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

//异常总数
- (void)loadSignTotalData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
        //指令、寻车、移车、提车
        [param setObject:self.abnormalModel.orderNo.length > 0 ? self.abnormalModel.orderNo : @(0) forKey:@"orderNo"];
        [param setObject:self.abnormalModel.vin.length > 0 ? self.abnormalModel.vin : @(0) forKey:@"vin"];
    }else if ([self.taskType integerValue] == 40) {
        //收车质检
        [param setObject:self.testingModel.ownerOrderNo.length > 0 ? self.testingModel.ownerOrderNo : @(0) forKey:@"orderNo"];
        [param setObject:self.testingModel.lotNo1.length > 0 ? self.testingModel.lotNo1 : @(0) forKey:@"vin"];
    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60) {
        //分配入库、出库确认、备料任务、装车交验
       [param setObject:self.orderNoAlloc.length > 0 ? self.orderNoAlloc : @(0) forKey:@"orderNo"];
        [param setObject:self.vinAlloc.length > 0 ? self.vinAlloc : @(0) forKey:@"vin"];
    }
    

    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:ExceptionTotal params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                weakSelf.exceptTotal = responseObject[@"data"];
                [weakSelf.tableView reloadData];
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

//九宫格异常标记
- (void)loadSignData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
        //指令、寻车、移车、提车任务
        [param setObject:self.abnormalModel.orderNo.length > 0 ? self.abnormalModel.orderNo : @(0) forKey:@"orderNo"];
        [param setObject:self.abnormalModel.vin.length > 0 ? self.abnormalModel.vin : @(0) forKey:@"vin"];
        
    }else if ([self.taskType integerValue] == 40){
        //收车质检
        [param setObject:self.testingModel.ownerOrderNo.length > 0 ? self.testingModel.ownerOrderNo : @(0) forKey:@"orderNo"];
        [param setObject:self.testingModel.lotNo1.length > 0 ? self.testingModel.lotNo1 : @(0) forKey:@"vin"];

    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60) {
        //分配入库、出库确认、备料任务、装车交验
        [param setObject:self.orderNoAlloc.length > 0 ? self.orderNoAlloc : @(0) forKey:@"orderNo"];
        [param setObject:self.vinAlloc.length > 0 ? self.vinAlloc : @(0) forKey:@"vin"];
    }
    
    [param setObject:self.taskType forKey:@"taskType"];
    
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:ExceptionInfo params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                NSArray *dataArray = responseObject[@"data"];
                NSMutableArray *tempA = [NSMutableArray array];
                for (int i = 0; i < 9; i++) {
                    ZCAbnormalInfoModel *infoModel = [ZCAbnormalInfoModel yy_modelWithDictionary:dataArray[i]];
                    [tempA addObject:infoModel];
                    weakSelf.testingStatus += [infoModel.countExcp integerValue];
                }
                weakSelf.exceptInfoArray = [tempA copy];
                [tempA removeAllObjects];
                for (int i = 9; i < dataArray.count; i++) {
                    ZCAbnormalInfoModel *infoModel = [ZCAbnormalInfoModel yy_modelWithDictionary:dataArray[i]];
                    [tempA addObject:infoModel];
                    weakSelf.testingStatus += [infoModel.countExcp integerValue];
                }
                weakSelf.cloakInfoArray = [tempA copy];
                [weakSelf.tableView reloadData];
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.orderNoAlloc.length > 0 || self.abnormalModel.orderNo.length > 0 || self.testingModel.ownerOrderNo.length > 0) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        //收车质检、分配入库、出库确认、备料任务
        if ([self.taskType integerValue] == 40 || [self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {
            return 6;
        }
        //指令、寻车、移车、提车任务
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30 || [self.taskType integerValue] == 60) {
            //指令、寻车、移车、提车任务、装车交验
            if (indexPath.row == 3 || indexPath.row == 1) {
                ZCAbnormalMissingTableViewCell *missCell = [tableView dequeueReusableCellWithIdentifier:missingCell forIndexPath:indexPath];
                missCell.selectionStyle = UITableViewCellSelectionStyleNone;
                if (indexPath.row == 1) {
                    if ([self.taskType integerValue] == 60) { //装车交验
                        missCell.content = self.vinAlloc;
                    }else {
                         missCell.content = self.abnormalModel.vin;
                    }
                    missCell.contentColor = ZCColor(0x000000, 0.87);
                    missCell.title = @"车架号";
                }else if (indexPath.row == 3) {
                    missCell.content = self.missingPart.length > 0 ? self.missingPart : @"请选择";
                    missCell.contentColor = ZCColor(0x000000, 0.54);
                }
                return missCell;
            }else if (indexPath.row == 0 || indexPath.row == 2){
                ZCTaskTotalNumTableViewCell *infoCell = [tableView dequeueReusableCellWithIdentifier:orderInfoCell forIndexPath:indexPath];
                infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
                infoCell.titleColor = ZCColor(0x000000, 0.54);
                infoCell.titleFont = [UIFont systemFontOfSize:FontSize(30)];
                infoCell.contentFont = [UIFont systemFontOfSize:FontSize(30)];
                infoCell.contentColor = ZCColor(0x000000, 0.87);
                if (indexPath.row == 0) {
                    infoCell.title = @"订单号";
                    if ([self.taskType integerValue] == 60) { //装车交验
                        infoCell.content = self.orderNoAlloc;
                    }else {
                        infoCell.content = self.abnormalModel.orderNo;
                    }
                }else if (indexPath.row == 2) {
                    infoCell.title = @"车型";
                    if ([self.taskType integerValue] == 60) {  //装车交验
                        infoCell.content = self.vehicleAlloc;
                    }else {
                        infoCell.content = self.abnormalModel.vehicle;
                    }
                }
                return infoCell;
            }else {
                ZCAbnormalSendTableViewCell *sendCell = [tableView dequeueReusableCellWithIdentifier:sendCellID forIndexPath:indexPath];
                sendCell.selectionStyle = UITableViewCellSelectionStyleNone;
                sendCell.delegate = self;
                if ([self.taskType integerValue] == 60) {
                    //装车校验
                    sendCell.send = self.isCanSend;
                }else {
                    sendCell.send = self.abnormalModel.isCanSend;
                }
                return sendCell;
            }
        }else {
            //收车质检、入库分配、出库确认、备料任务
            if (indexPath.row == 4 || indexPath.row == 1) {
                ZCAbnormalMissingTableViewCell *missCell = [tableView dequeueReusableCellWithIdentifier:missingCell forIndexPath:indexPath];
                missCell.selectionStyle = UITableViewCellSelectionStyleNone;
                if (indexPath.row == 1) {
                    if ([self.taskType integerValue] == 40) {   //收车质检
                        missCell.content = self.testingModel.lotNo1;
                    }else if ([self.taskType integerValue] == 40 || [self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {   //入库分配、出库确认、备料任务
                        missCell.content = self.vinAlloc;
                    }
                    missCell.contentColor = ZCColor(0x000000, 0.87);
                    missCell.title = @"车架号";
                }else if (indexPath.row == 4) {
                    missCell.content = self.missingPart.length > 0 ? self.missingPart : @"请选择";
                    missCell.contentColor = ZCColor(0x000000, 0.54);
                }
                return missCell;
            }else if (indexPath.row == 5) {
                ZCAbnormalSendTableViewCell *sendCell = [tableView dequeueReusableCellWithIdentifier:sendCellID forIndexPath:indexPath];
                sendCell.selectionStyle = UITableViewCellSelectionStyleNone;
                sendCell.delegate = self;
                if ([self.taskType integerValue] == 40) {
                    sendCell.send = self.testingModel.isCanSend;
                }else {
                    sendCell.send = self.isCanSend;
                }
                return sendCell;
            }else {
                ZCTaskTotalNumTableViewCell *infoCell = [tableView dequeueReusableCellWithIdentifier:orderInfoCell forIndexPath:indexPath];
                infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
                infoCell.titleColor = ZCColor(0x000000, 0.54);
                infoCell.titleFont = [UIFont systemFontOfSize:FontSize(30)];
                infoCell.contentFont = [UIFont systemFontOfSize:FontSize(30)];
                infoCell.contentColor = ZCColor(0x000000, 0.87);
                if (indexPath.row == 0) {
                    infoCell.title = @"订单号";
                    if ([self.taskType integerValue] == 40) {   //收车质检
                        infoCell.content = self.testingModel.ownerOrderNo;
                    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {    //入库分配、出库确认、备料任务
                        infoCell.content = self.orderNoAlloc;
                    }
                }else if (indexPath.row == 2) {
                    infoCell.title = @"车型";
                    if ([self.taskType integerValue] == 40) {     //收车质检
                        infoCell.content = self.testingModel.materielId;
                    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {      //入库分配、出库确认、备料任务
                        infoCell.content = self.vehicleAlloc;
                    }
                }else if (indexPath.row == 3) {
                    infoCell.title = @"收货仓库";
                    if ([self.taskType integerValue] == 40) {     //收车质检
                        infoCell.content = self.testingModel.storeHouseName;
                    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {      //入库分配、出库确认、备料任务
                        infoCell.content = self.storageAlloc;
                    }
                }
                return infoCell;
            }
        }
       
    }else {
        ZCAbnormalSignTableViewCell *SignPartCell = [tableView dequeueReusableCellWithIdentifier:signCell forIndexPath:indexPath];
        SignPartCell.selectionStyle = UITableViewCellSelectionStyleNone;
        SignPartCell.delegate = self;
        SignPartCell.exceptTotal = self.exceptTotal;
        SignPartCell.exceptArray = self.exceptInfoArray;
        SignPartCell.cloakArray = self.cloakInfoArray;
        return SignPartCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return space(90);
    }
    return space(1214);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return space(20);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            ZCVinQRCodeViewController *vinQRVC = [[ZCVinQRCodeViewController alloc] init];
            if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
                //指令、寻车、移车、提车任务
                vinQRVC.vin = self.abnormalModel.vin;
            }else if ([self.taskType integerValue] == 40) {
                //收车质检
                vinQRVC.vin = self.testingModel.lotNo1;
            }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60) {
                //入库分配、出库确认、备料任务、装车交验
                vinQRVC.vin = self.vinAlloc;
            }
            
            [self.navigationController pushViewController:vinQRVC animated:YES];
        }else {
            if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30 || [self.taskType integerValue] == 60) {
                //指令、寻车、移车、提车任务、装车交验
                if(indexPath.row == 3) {
                    ZCAbnormalLackViewController *lackVC = [[ZCAbnormalLackViewController alloc] init];
                    if ([self.taskType integerValue] == 60) { //装车交验
                        lackVC.vin = self.vinAlloc;
                        lackVC.orderNo = self.orderNoAlloc;
                        lackVC.taskId = self.releaseId;
                        lackVC.taskType = self.taskType;
                    }else {
                        lackVC.vin = self.abnormalModel.vin;
                        lackVC.orderNo = self.abnormalModel.orderNo;
                        lackVC.taskId = [self.taskType integerValue] == 0 ? self.abnormalModel.releaseId : self.abnormalModel.taskId;
                        lackVC.taskType = self.abnormalModel.taskType;
                    }
                    
                    __weak typeof(self)weakSelf = self;
                    lackVC.lackBlock = ^(NSString *missimgPart) {
                        weakSelf.missingPart = missimgPart;
                    };
                    [self.navigationController pushViewController:lackVC animated:YES];
                }
            }else {
                //收车质检、分配入库、出库确认、备料任务
                if(indexPath.row == 4) {
                    ZCAbnormalLackViewController *lackVC = [[ZCAbnormalLackViewController alloc] init];
                    if ([self.taskType integerValue] == 40) {  //收车质检
                        lackVC.vin = self.testingModel.lotNo1;
                        lackVC.orderNo = self.testingModel.ownerOrderNo;
                        lackVC.taskId = self.testingModel.Id;
                        lackVC.taskType = self.testingModel.taskType;
                    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50) {    //分配入库、 出库确认、备料任务
                        lackVC.vin = self.vinAlloc;
                        lackVC.orderNo = self.orderNoAlloc;
                        lackVC.taskId = self.releaseId;
                        lackVC.taskType = self.taskType;
                    }
                    
                    __weak typeof(self)weakSelf = self;
                    lackVC.lackBlock = ^(NSString *missimgPart) {
                        weakSelf.missingPart = missimgPart;
                    };
                    [self.navigationController pushViewController:lackVC animated:YES];
                }
            }
        }
        
    }
}

#pragma mark - ZCAbnormalSendTableViewCellDelegate
- (void)abnormalSendTableViewCellSendButtonSelected:(BOOL)selected {
    self.sendOrNot = @"Y";    
}

- (void)abnormalSendTableViewCellNotSendButtonSelected:(BOOL)selected {
    self.sendOrNot = @"N";
}

#pragma mark - ZCAbnormalSignTableViewCellDelegate
- (void)abnormalSignTableViewCellSignMissingButtonClickWithIndex:(NSIndexPath *)index isloack:(BOOL)loack{
    
    ZCAbnormalInfoModel *infoM = nil;
    if (loack) {
        infoM = (ZCAbnormalInfoModel *)self.cloakInfoArray[index.item];
    }else {
        infoM = (ZCAbnormalInfoModel *)self.exceptInfoArray[index.item];
    }
    ZCAbnormalDetailViewController *detailVC = [[ZCAbnormalDetailViewController alloc] init];
    detailVC.infoModel = infoM;
    
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
        //指令、寻车、移车、提车任务
        detailVC.orderNo = self.abnormalModel.orderNo;
        detailVC.vin = self.abnormalModel.vin;
        detailVC.taskId = self.abnormalModel.releaseId;
        detailVC.taskType = self.abnormalModel.taskType;
    }else if ([self.taskType integerValue] == 40) {
        //收车质检
        detailVC.orderNo = self.testingModel.ownerOrderNo;
        detailVC.vin = self.testingModel.lotNo1;
        detailVC.taskId = self.testingModel.Id;
        detailVC.taskType = self.testingModel.taskType;
    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60) {
        //分配入库、出库确认、备料任务、装车交验
        detailVC.orderNo = self.orderNoAlloc;
        detailVC.vin = self.vinAlloc;
        detailVC.taskId = self.releaseId;
        detailVC.taskType = self.taskType;
    }
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)abnormalSignTableViewCellLoackShow:(BOOL)show {
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30 || [self.taskType integerValue] == 60) {
        //指令、寻车、移车、提车任务、装车交验
        if (show) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            self.tableView.scrollEnabled = YES;
        }else {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            self.tableView.scrollEnabled = YES;
        }
    }else {
        //收车质检、分配入库、出库确认、备料任务
        if (!show) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }

    }
    
}

- (void)abnormalSignTableViewCellAllSignButtonClick {
    ZCAbnormalAllViewController *allVC = [[ZCAbnormalAllViewController alloc] init];
    if ([self.taskType integerValue] == 0 || [self.taskType integerValue] == 10 || [self.taskType integerValue] == 20 || [self.taskType integerValue] == 30) {
        //指令、寻车、移车、提车任务
        allVC.orderNo = self.abnormalModel.orderNo;
        allVC.vin = self.abnormalModel.vin;
    }else if ([self.taskType integerValue] == 40) {
        //收车质检
        allVC.orderNo = self.testingModel.ownerOrderNo;
        allVC.vin = self.testingModel.lotNo1;
    }else if ([self.taskType integerValue] == 42 || [self.taskType integerValue] == 51 || [self.taskType integerValue] == 50 || [self.taskType integerValue] == 60) {
        //分配入库、出库确认、备料任务、装车交验
        allVC.orderNo = self.orderNoAlloc;
        allVC.vin = self.vinAlloc;
    }
    
    [self.navigationController pushViewController:allVC animated:YES];
    
}



@end
