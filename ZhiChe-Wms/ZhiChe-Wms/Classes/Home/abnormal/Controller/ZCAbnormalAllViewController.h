//
//  ZCAbnormalAllViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/24.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCAbnormalSignListViewDelegate <NSObject>

- (void)abnormalSignListViewDeleteButtonClickWithCellIndex:(NSIndexPath *)index;
- (void)abnormalSignListViewScanActionWithImaA:(NSMutableArray *)imaga index:(NSIndexPath *)index;
- (void)abnormalSignListViewLongPressImageWithSignArray:(NSArray *)signArray;

@end

@interface ZCAbnormalAllViewController : UIViewController

@property (nonatomic, weak) id<ZCAbnormalSignListViewDelegate> delegate;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, copy) NSString *vin;

@end
