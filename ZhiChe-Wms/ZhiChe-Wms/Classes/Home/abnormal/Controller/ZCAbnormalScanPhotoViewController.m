//
//  ZCAbnormalScanPhotoViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/23.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalScanPhotoViewController.h"
#import "ZCAbnormalSignListCollectionViewCell.h"
#import "ZCAbnormalModel.h"

static NSString *scanCellID = @"ZCAbnormalSignListCollectionViewCell";

@interface ZCAbnormalScanPhotoViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collection;
@property (nonatomic, assign) CGFloat startX;
@property (nonatomic, assign) BOOL bMove;

@end

@implementation ZCAbnormalScanPhotoViewController
#pragma mark - 懒加载
- (UICollectionView *)collection {
    if (!_collection) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, SCREENHEIGHT-64) collectionViewLayout:flowLayout];
        _collection.delegate = self;
        _collection.dataSource = self;
        _collection.showsHorizontalScrollIndicator = NO;
//        _collection.scrollEnabled = NO;
        
        [_collection registerClass:[ZCAbnormalSignListCollectionViewCell class] forCellWithReuseIdentifier:scanCellID];
    }
    return _collection;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = @"图片预览";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collection];
    [self.collection scrollToItemAtIndexPath:self.nowIndex atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - 按钮点击
- (void)backToHome {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCAbnormalSignListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:scanCellID forIndexPath:indexPath];
    ZCAbnormalListDetailModel *detailM = self.dataArray[indexPath.item];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:detailM.picUrl]];
    
    return cell;
}

#pragma mark - UICollectionViewFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREENWIDTH, SCREENHEIGHT-64);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//滑动开始事件
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //    NSLog(@"开始了");
    UITouch *touch = [touches anyObject];
    CGPoint pointone = [touch locationInView:self.view];//获得初始的接触点
    //以字符的形式输出触摸点
    
    
    _startX  = pointone.x;
    NSLog(@"触摸点的坐标：%f",_startX);
}
//滑动移动事件
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //    NSLog(@"移动啦");
    UITouch *touch = [touches anyObject];
    //imgViewTop是滑动后最后接触的View
    CGPoint pointtwo = [touch locationInView:self.view];  //获得滑动后最后接触屏幕的点
    NSLog(@"移动点的坐标：%f,%f",pointtwo.x,_startX);
    int position = (pointtwo.x-_startX);
    NSLog(@"position is %d",position);
    if(fabs(pointtwo.x-_startX)>100)
    {  //判断两点间的距离
        NSLog(@"移动了");
        _bMove = YES;
    }
}

//滑动结束处理事件
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint pointtwo = [touch locationInView:self.view];  //获得滑动后最后接触屏幕的点
    if((fabs(pointtwo.x-_startX)>50)&&(_bMove))
    {
        //判断点的位置关系 左滑动
        if(pointtwo.x-_startX>0)
        {   //左向右滑动业务处理
            NSLog(@"左向右移动");
            NSInteger num = self.nowIndex.item - 1;
            if (self.dataArray.count > num) {
                [self.collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:num inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
            }
            
        }
        //判断点的位置关系 右滑动
        else
        {  //右向左滑动业务处理
            NSLog(@"右移动");
            NSInteger num = self.nowIndex.item + 1;
            if (self.dataArray.count > num) {
                [self.collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:num inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
        }
    }
}



@end
