//
//  ZCAbnormalSignViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCAbnormalSignViewController : UIViewController
@property (nonatomic, copy) NSString *releaseId;
@property (nonatomic, copy) NSString *taskType;
@property (nonatomic, copy) NSString *visitType;
@property (nonatomic, copy) NSString *orderNoAlloc;
@property (nonatomic, copy) NSString *vinAlloc;
@property (nonatomic, copy) NSString *vehicleAlloc;
@property (nonatomic, copy) NSString *storageAlloc;
@property (nonatomic, copy) NSString *isCanSend;

@end
