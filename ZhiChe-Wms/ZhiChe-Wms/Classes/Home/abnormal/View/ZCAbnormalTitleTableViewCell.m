//
//  ZCAbnormalTitleTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalTitleTableViewCell.h"

@interface ZCAbnormalTitleTableViewCell ()
@property (nonatomic, strong) UIView *markView;
@property (nonatomic, strong) UILabel *titleLable;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation ZCAbnormalTitleTableViewCell
#pragma mark - 懒加载
- (UIView *)markView {
    if (!_markView) {
        _markView = [[UIView alloc] init];
        _markView.backgroundColor = ZCColor(0xff8213, 1);
    }
    return _markView;
}

- (UILabel *)titleLable {
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.text = @"前保险杠";
        _titleLable.textColor = ZCColor(0x000000, 0.87);
        _titleLable.font = [UIFont systemFontOfSize:FontSize(26)];
        _titleLable.numberOfLines = 0;
    }
    return _titleLable;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.markView];
        [self addSubview:self.titleLable];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLable.text = title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.markView.hidden = NO;
    }else {
        self.markView.hidden = YES;
    }
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [self.markView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(space(8), space(32)));
    }];
    
    [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.markView.mas_right).offset(space(10));
        make.centerY.equalTo(weakSelf.markView.mas_centerY);
        make.width.mas_equalTo(space(170));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}

@end
