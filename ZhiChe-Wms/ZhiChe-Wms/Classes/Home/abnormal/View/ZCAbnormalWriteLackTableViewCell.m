//
//  ZCAbnormalWriteLackTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/25.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalWriteLackTableViewCell.h"

@interface ZCAbnormalWriteLackTableViewCell ()<UITextViewDelegate>
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *placeholderLabel;
@end

@implementation ZCAbnormalWriteLackTableViewCell
#pragma mark - 懒加载
- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, space(180))];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.delegate = self;
        _textView.font = [UIFont systemFontOfSize:FontSize(26)];
        _textView.textColor = ZCColor(0x000000, 0.87);
        _textView.textContainerInset = UIEdgeInsetsMake(space(40), space(40), space(40), space(40));
        _textView.returnKeyType = UIReturnKeyDone;
    }
    return _textView;
}

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(space(20), space(20), space(300), space(60))];
        _placeholderLabel.textColor = ZCColor(0x000000, 0.34);
        _placeholderLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _placeholderLabel;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.textView];
        [self.textView addSubview:self.placeholderLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setMissingModel:(ZCAbnormalMissingModel *)missingModel {
    _missingModel = missingModel;
    NSMutableString *otherPart = [NSMutableString string];
    for (int i = 0; i < missingModel.childComponent.count; i++) {
        ZCAbnormalMisssingPartModel *partModel = (ZCAbnormalMisssingPartModel *)missingModel.childComponent[i];
        if (i == 0) {
            if ([partModel.code integerValue] == 5001001) {
                if (partModel.describe.length > 0) {
                     [otherPart appendString:partModel.describe];
                }else {
                     [otherPart appendString:partModel.name];
                }
            }
           
        }else {
            [otherPart appendString:@"、"];
            [otherPart appendString:partModel.describe];
        }
    }
    [otherPart appendString:@"(点击可编辑)"];
    self.textView.text = otherPart;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    [textView becomeFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(abnormalWriteLackTableViewCellTextView: missingModel:)]) {
        [self.delegate abnormalWriteLackTableViewCellTextView:textView.text missingModel:self.missingModel];
    }
}

//- (void)keyboardWillShow:(NSNotification *)noti {
//    if ([self.delegate respondsToSelector:@selector(abnormalWriteLackTableViewCellKeyboardWillShow:)]) {
//        CGRect rect = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        [self.delegate abnormalWriteLackTableViewCellKeyboardWillShow:rect];
//    }
//}


@end
