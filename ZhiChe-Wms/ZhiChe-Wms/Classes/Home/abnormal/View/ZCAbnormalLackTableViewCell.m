//
//  ZCAbnormalLackTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/24.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalLackTableViewCell.h"

@interface ZCAbnormalLackTableViewCell ()
@property (nonatomic, assign) CGFloat currentW;
@property (nonatomic, assign) CGFloat currentX;
@property (nonatomic, assign) CGFloat currentY;
@end

@implementation ZCAbnormalLackTableViewCell
#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.currentX = space(40);
        self.currentY = space(40);
    }
    return self;
}

#pragma mark - 属性方法
- (void)setMissingModel:(ZCAbnormalMissingModel *)missingModel {
    for (UIButton *btn in self.subviews) {
        [btn removeFromSuperview];
    }
    _missingModel = missingModel;
    for (int i = 0; i < missingModel.childComponent.count; i++) {
        ZCAbnormalMisssingPartModel *partM = missingModel.childComponent[i];
        //计算button的宽高
        NSMutableDictionary *attr = [NSMutableDictionary dictionary];
        [attr setObject:[UIFont systemFontOfSize:FontSize(26)] forKey:NSFontAttributeName];
        CGSize size = [partM.name boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, space(68)) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attr context:nil].size;
        if (i == 0) {
            self.currentX = space(40);
            self.currentY = space(40);
        }else {
            self.currentX = self.currentW;
        }
        self.currentW = self.currentX + size.width + space(40) + space(40);
       
        if (self.currentW > SCREENWIDTH) {
            self.currentX = space(40);
            self.currentW = self.currentX + size.width + space(40) + space(40);
            self.currentY = self.currentY + space(40) + space(68);
        }
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(self.currentX, self.currentY, size.width + space(40), space(68))];
        [button setTitle:partM.name forState:UIControlStateNormal];
        [button setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        [button setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateSelected];
        [button setBackgroundColor:ZCColor(0xffffff, 1)];
//        if (partM.selected) {
//            button.selected = partM.selected;
//            button.backgroundColor = ZCColor(0xff8213, 0.6);
//        }
        if ([partM.isReigister boolValue]) {
            button.selected = [partM.isReigister boolValue];
            button.backgroundColor = ZCColor(0xff8213, 0.6);
        }
        button.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        button.tag = [partM.code integerValue];
        button.layer.masksToBounds = YES;
        button.layer.borderColor = ZCColor(0xdadada, 1).CGColor;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = space(6);
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
    missingModel.rowHeight = self.currentY + space(40) + space(68);
    [self layoutIfNeeded];
}

#pragma mark - 按钮点击
- (void)buttonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    for (ZCAbnormalMisssingPartModel *sissingM in self.missingModel.childComponent) {
        if ([sissingM.code integerValue] == sender.tag) {
            sissingM.selected = sender.selected;
        }
    }
    if (sender.selected) {
        sender.backgroundColor = ZCColor(0xff8213, 0.6);
    }else {
        sender.backgroundColor = ZCColor(0xffffff, 1);
    }
    if ([self.delegate respondsToSelector:@selector(abnormalLackTableViewCellSelectLackButton:missingModel:)]) {
        [self.delegate abnormalLackTableViewCellSelectLackButton:sender missingModel:self.missingModel];
    }
}


@end
