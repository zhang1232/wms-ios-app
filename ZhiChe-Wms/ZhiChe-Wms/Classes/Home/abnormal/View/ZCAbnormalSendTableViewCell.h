//
//  ZCAbnormalSendTableViewCell.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/11/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZCAbnormalSendTableViewCellDelegate <NSObject>

- (void)abnormalSendTableViewCellSendButtonSelected:(BOOL)selected;
- (void)abnormalSendTableViewCellNotSendButtonSelected:(BOOL)selected;

@end

@interface ZCAbnormalSendTableViewCell : UITableViewCell

@property (nonatomic, weak) id<ZCAbnormalSendTableViewCellDelegate> delegate;
@property (nonatomic, copy) NSString *send;

@end

NS_ASSUME_NONNULL_END
