//
//  ZCAbnormalSendTableViewCell.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/11/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSendTableViewCell.h"

@interface ZCAbnormalSendTableViewCell ()

@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UIButton *notSendButton;

@end

@implementation ZCAbnormalSendTableViewCell

- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [[UIButton alloc] init];
        [_sendButton setTitle:@"异常发运" forState:UIControlStateNormal];
        [_sendButton setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        [_sendButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateSelected];
        [_sendButton setBackgroundColor:ZCColor(0xffffff, 1)];
        _sendButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _sendButton.layer.masksToBounds = YES;
        _sendButton.layer.borderColor = ZCColor(0xdadada, 1).CGColor;
        _sendButton.layer.borderWidth = 1;
        _sendButton.layer.cornerRadius = space(6);
        [_sendButton addTarget:self action:@selector(sendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}

- (UIButton *)notSendButton {
    if (!_notSendButton) {
        _notSendButton = [[UIButton alloc] init];
        [_notSendButton setTitle:@"异常不发运" forState:UIControlStateNormal];
        [_notSendButton setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        [_notSendButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateSelected];
        [_notSendButton setBackgroundColor:ZCColor(0xffffff, 1)];
        _notSendButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _notSendButton.layer.masksToBounds = YES;
        _notSendButton.layer.borderColor = ZCColor(0xdadada, 1).CGColor;
        _notSendButton.layer.borderWidth = 1;
        _notSendButton.layer.cornerRadius = space(6);
        [_notSendButton addTarget:self action:@selector(notSendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _notSendButton;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.sendButton];
        [self addSubview:self.notSendButton];
    }
    return self;
}

- (void)setSend:(NSString *)send {
    _send = send;
    if ([send isEqualToString:@"Y"]) {
        [self sendButtonClick:self.sendButton];
    }else if ([send isEqualToString:@"N"]) {
        [self notSendButtonClick:self.notSendButton];
    }
}

- (void)sendButtonClick:(UIButton *)sender {
    sender.selected = YES;
    [sender setBackgroundColor:ZCColor(0xff8213, 0.6)];
    self.notSendButton.selected = NO;
    [self.notSendButton setBackgroundColor:ZCColor(0xffffff, 1)];
    
    if ([self.delegate respondsToSelector:@selector(abnormalSendTableViewCellSendButtonSelected:)]) {
        [self.delegate abnormalSendTableViewCellSendButtonSelected:sender.selected];
    }
    
}

- (void)notSendButtonClick:(UIButton *)sender {
    sender.selected = YES;
    [sender setBackgroundColor:ZCColor(0xff8213, 0.6)];
    self.sendButton.selected = NO;
    [self.sendButton setBackgroundColor:ZCColor(0xffffff, 1)];
    
    if ([self.delegate respondsToSelector:@selector(abnormalSendTableViewCellNotSendButtonSelected:)]) {
        [self.delegate abnormalSendTableViewCellNotSendButtonSelected:sender.selected];
    }
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_left).offset(SCREENWIDTH * 1 / 4);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo(space(300));
    }];
    
    [_notSendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_left).offset(SCREENWIDTH * 3 / 4);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo(space(300));
    }];
}

@end
