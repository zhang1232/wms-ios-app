//
//  ZCAbnormalSignListView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/22.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignListView.h"
#import "ZCAbnormalSignListTableViewCell.h"
#import "ZCAbnormalModel.h"

static NSString *signListCellID = @"ZCAbnormalSignListTableViewCell";

@interface ZCAbnormalSignListView ()<UITableViewDelegate, UITableViewDataSource, ZCAbnormalSignListTableViewCellDelegate>

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIButton *deleteAllButton;

@end

@implementation ZCAbnormalSignListView
#pragma mark - 懒加载
- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, space(90))];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"标记的异常";
        _titleLabel.textColor = ZCColor(0xff8213, 1);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _titleLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, space(90), self.bounds.size.width, self.bounds.size.height - space(90)) style:UITableViewStylePlain];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerClass:[ZCAbnormalSignListTableViewCell class] forCellReuseIdentifier:signListCellID];
    }
    return _tableView;
}

- (UIButton *)deleteAllButton {
    if (!_deleteAllButton) {
        _deleteAllButton = [[UIButton alloc] init];
        [_deleteAllButton setTitle:@"全部删除" forState:UIControlStateNormal];
        [_deleteAllButton setTitleColor:ZCColor(0xff8213, 1) forState:UIControlStateNormal];
        _deleteAllButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        [_deleteAllButton addTarget:self action:@selector(deleteAllSign:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteAllButton;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.headerView];
        [self addSubview:self.tableView];
        [self.headerView addSubview:self.titleLabel];
        [self.headerView addSubview:self.deleteAllButton];
        [self.headerView addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setSignArray:(NSArray *)signArray {
    _signArray = signArray;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.signArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCAbnormalSignListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:signListCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.indexPath = indexPath;
    ZCAbnormalSignModel *signModel = (ZCAbnormalSignModel *)self.signArray[indexPath.row];
    cell.name = [NSString stringWithFormat:@"%@ - %@",signModel.level2Name,signModel.level3Name];
    cell.imageArray = [signModel.exceptionDetails mutableCopy];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return space(140);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

#pragma mark - 按钮点击事件
- (void)deleteAllSign:(UIButton *)sender {
    NSMutableArray *signA = [self.signArray mutableCopy];
    [signA removeAllObjects];
    self.signArray = [signA copy];
    [self.tableView reloadData];
    if ([self.delegate respondsToSelector:@selector(abnormalSignListViewLongPressImageWithSignArray:)]) {
        [self.delegate abnormalSignListViewLongPressImageWithSignArray:self.signArray];
    }
}

#pragma mark - ZCAbnormalSignListTableViewCellDelegate
- (void)abnormalSignListTableViewCellDeleteButtonClickWithIndex:(NSIndexPath *)index {
    //点击删除按钮删除本条数据
    //数据
    ZCAbnormalSignModel *signM = (ZCAbnormalSignModel *)self.signArray[index.row];
    NSMutableArray *signA = [self.signArray mutableCopy];
    [signA removeObject:signM];
    self.signArray = [signA copy];
    
    //界面
    [self.tableView reloadData];
    
    if ([self.delegate respondsToSelector:@selector(abnormalSignListViewLongPressImageWithSignArray:)]) {
        [self.delegate abnormalSignListViewLongPressImageWithSignArray:self.signArray];
    }
}

- (void)abnormalSignListTableViewCellScanPhotoWithCellIndex:(NSIndexPath *)cellIndex itemIndex:(NSIndexPath *)itemIndex {
    ZCAbnormalSignModel *signM = (ZCAbnormalSignModel *)self.signArray[cellIndex.row];
    if ([self.delegate respondsToSelector:@selector(abnormalSignListViewScanActionWithImaA:index:)]) {
        [self.delegate abnormalSignListViewScanActionWithImaA:[signM.exceptionDetails mutableCopy] index:itemIndex];
    }
}

- (void)abnormalSignListTableViewCellLongPressImageWithCellIndex:(NSIndexPath *)cellIndex itemIndex:(NSIndexPath *)itemIndex {
    ZCAbnormalSignModel *signM = (ZCAbnormalSignModel *)self.signArray[cellIndex.row];
    ZCAbnormalListDetailModel *detailM = (ZCAbnormalListDetailModel *)signM.exceptionDetails[itemIndex.item];
    NSMutableArray *tempA = [signM.exceptionDetails mutableCopy];
    [tempA removeObject:detailM];
    signM.exceptionDetails = [tempA copy];
    if (signM.exceptionDetails.count == 0) {
        NSMutableArray *signA = [self.signArray mutableCopy];
        [signA removeObject:signM];
        self.signArray = [signA copy];
    }
    [self.tableView reloadData];
    if ([self.delegate respondsToSelector:@selector(abnormalSignListViewLongPressImageWithSignArray:)]) {
        [self.delegate abnormalSignListViewLongPressImageWithSignArray:self.signArray];
    }
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.headerView.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.headerView.mas_centerY);
    }];
    
    [_deleteAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.headerView.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.headerView.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.headerView.mas_left);
        make.right.equalTo(weakSelf.headerView.mas_right);
        make.bottom.equalTo(weakSelf.headerView.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}


@end
