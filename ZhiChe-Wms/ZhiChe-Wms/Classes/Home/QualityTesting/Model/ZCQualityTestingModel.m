//
//  ZCQualityTestingModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/30.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCQualityTestingModel.h"

@implementation ZCQualityTestingModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end
