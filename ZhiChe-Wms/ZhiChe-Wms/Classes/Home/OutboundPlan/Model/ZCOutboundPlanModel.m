//
//  ZCOutboundPlanModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/5.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCOutboundPlanModel.h"

@implementation ZCOutboundPlanInfoModel

@end

@implementation ZCOutboundPlanConditionModel

@end

@implementation ZCOutboundPlanModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCOutboundPlanInfoModel class]};
}

@end

@implementation ZCOutboundPlanDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end
