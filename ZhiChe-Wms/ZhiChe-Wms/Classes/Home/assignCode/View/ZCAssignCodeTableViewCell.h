//
//  ZCAssignCodeTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCAssignCodeModel.h"

@interface ZCAssignCodeTableViewCell : UITableViewCell
@property (nonatomic, strong) ZCAssignCodeModel *assignCodeModel;
@property (nonatomic, copy) NSString *typeTitle;
@property (nonatomic, copy) NSString *statusTitle;
@end
