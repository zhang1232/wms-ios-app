//
//  ZCAssignCodeTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAssignCodeTableViewCell.h"

@interface ZCAssignCodeTableViewCell ()
@property (nonatomic, strong) UILabel *orderTitleLabel;
@property (nonatomic, strong) UILabel *orderNumLabel;
@property (nonatomic, strong) UIImageView *startImageView;
@property (nonatomic, strong) UILabel *startLabel;
@property (nonatomic, strong) UILabel *typeTitleLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UIImageView *endImageView;
@property (nonatomic, strong) UILabel *endLabel;
@property (nonatomic, strong) UILabel *statusTitleLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *carTitleLabel;
@property (nonatomic, strong) UILabel *carNumLabel;

@end

@implementation ZCAssignCodeTableViewCell
#pragma mark - 懒加载
- (UILabel *)orderTitleLabel {
    if (!_orderTitleLabel) {
        _orderTitleLabel = [[UILabel alloc] init];
        _orderTitleLabel.text = @"订单号";
        _orderTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _orderTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderTitleLabel;
}

- (UILabel *)orderNumLabel {
    if (!_orderNumLabel) {
        _orderNumLabel = [[UILabel alloc] init];
        _orderNumLabel.text = @"20288837";
        _orderNumLabel.textColor = ZCColor(0x000000, 0.87);
        _orderNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderNumLabel;
}

- (UIImageView *)startImageView {
    if (!_startImageView) {
        _startImageView = [[UIImageView alloc] init];
        _startImageView.image = [UIImage imageNamed:@"from"];
    }
    return _startImageView;
}

- (UILabel *)startLabel {
    if (!_startLabel) {
        _startLabel = [[UILabel alloc] init];
        _startLabel.text = @"全顺库";
        _startLabel.textColor = ZCColor(0x000000, 0.54);
        _startLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _startLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _startLabel.numberOfLines = 0;
    }
    return _startLabel;
}

- (UILabel *)typeTitleLabel {
    if (!_typeTitleLabel) {
        _typeTitleLabel = [[UILabel alloc] init];
        _typeTitleLabel.text = @"车型";
        _typeTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _typeTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _typeTitleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _typeTitleLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.text = @"提车任务";
        _typeLabel.textColor = ZCColor(0x000000, 0.34);
        _typeLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _typeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _typeLabel.numberOfLines = 0;
    }
    return _typeLabel;
}

- (UIImageView *)endImageView {
    if (!_endImageView) {
        _endImageView = [[UIImageView alloc] init];
        _endImageView.image = [UIImage imageNamed:@"to"];
    }
    return _endImageView;
}

- (UILabel *)endLabel {
    if (!_endLabel) {
        _endLabel = [[UILabel alloc] init];
        _endLabel.text = @"电网库";
        _endLabel.textColor = ZCColor(0x000000, 0.54);
        _endLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _endLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _endLabel.numberOfLines = 0;
    }
    return _endLabel;
}

- (UILabel *)statusTitleLabel {
    if (!_statusTitleLabel) {
        _statusTitleLabel = [[UILabel alloc] init];
        _statusTitleLabel.text = @"二维码";
        _statusTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _statusTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _statusTitleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _statusTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"创建";
        _statusLabel.textColor = ZCColor(0x000000, 0.34);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _startLabel.numberOfLines = 0;
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UILabel *)carTitleLabel {
    if (!_carTitleLabel) {
        _carTitleLabel = [[UILabel alloc] init];
        _carTitleLabel.text = @"车架号";
        _carTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _carTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carTitleLabel;
}

- (UILabel *)carNumLabel {
    if (!_carNumLabel) {
        _carNumLabel = [[UILabel alloc] init];
        _carNumLabel.text = @"-----";
        _carNumLabel.textColor = ZCColor(0x000000, 0.87);
        _carNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carNumLabel;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.orderTitleLabel];
        [self addSubview:self.orderNumLabel];
        [self addSubview:self.startImageView];
        [self addSubview:self.startLabel];
        [self addSubview:self.typeTitleLabel];
        [self addSubview:self.typeLabel];
        [self addSubview:self.endImageView];
        [self addSubview:self.endLabel];
        [self addSubview:self.statusTitleLabel];
        [self addSubview:self.statusLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.carTitleLabel];
        [self addSubview:self.carNumLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setAssignCodeModel:(ZCAssignCodeModel *)assignCodeModel {
    _assignCodeModel = assignCodeModel;
    self.orderNumLabel.text = assignCodeModel.orderNo;
    self.startLabel.text = assignCodeModel.originName;
    self.endLabel.text = assignCodeModel.destName;
    self.typeLabel.text = assignCodeModel.vehicle;
    self.statusLabel.text = assignCodeModel.qrCode;
    self.carNumLabel.text = assignCodeModel.vin;
}

- (void)setTypeTitle:(NSString *)typeTitle {
    _typeTitle = typeTitle;
    self.typeTitleLabel.text = typeTitle;
}

- (void)setStatusTitle:(NSString *)statusTitle {
    _statusTitle = statusTitle;
    self.statusTitleLabel.text = statusTitle;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_orderTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(40));
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.width.mas_equalTo(space(120));
    }];
    
    [_orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.orderTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderTitleLabel.mas_right).offset(space(20));
    }];
    
    [_startImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.top.equalTo(weakSelf.orderTitleLabel.mas_bottom).offset(space(40));
        make.size.mas_equalTo(CGSizeMake(space(26), space(32)));
    }];
    
    [_startLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startImageView.mas_centerY);
        make.left.equalTo(weakSelf.startImageView.mas_right).offset(space(20));
        make.right.equalTo(weakSelf.mas_centerX).offset(-space(20));
    }];
    
    [_typeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startLabel.mas_centerY);
        make.left.equalTo(weakSelf.mas_centerX).offset(space(20));
        make.width.mas_equalTo(space(140));
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startLabel.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.left.equalTo(weakSelf.typeTitleLabel.mas_right).offset(space(20));
    }];
    
    [_endImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.startImageView.mas_bottom).offset(space(30));
        make.centerX.equalTo(weakSelf.startImageView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(space(26), space(32)));
    }];
    
    [_endLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endImageView.mas_centerY);
        make.left.equalTo(weakSelf.startLabel.mas_left);
        make.right.equalTo(weakSelf.mas_centerX).offset(-space(20));
    }];
    
    [_statusTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeTitleLabel.mas_left);
        make.width.mas_equalTo(space(140));
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeLabel.mas_left);
        make.right.equalTo(weakSelf.typeLabel.mas_right);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.endImageView.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(2));
    }];
    
    [_carTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.lineView.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.orderTitleLabel.mas_left);
    }];
    
    [_carNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.carTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderNumLabel.mas_left);
    }];
}


@end
