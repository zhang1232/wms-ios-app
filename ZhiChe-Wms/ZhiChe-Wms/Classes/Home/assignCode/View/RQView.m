//
//  RQView.m
//  QRCode
//
//  Created by liguowei on 15/11/19.
//  Copyright © 2015年 liguiwei. All rights reserved.
//

#import "RQView.h"

@interface RQView () {
    int num;
    BOOL upOrdown;
    NSTimer *_timer;
}

@end

@implementation RQView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
    
        [self creatUI];
    }
    
    return self;
}

- (void)creatUI
{
    UIImageView *backImageView= [[UIImageView alloc]initWithFrame:self.frame];
    backImageView.image = [UIImage imageNamed:@"qrBack"];
    [self addSubview:backImageView];
    
    _line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 10)];
    _line.image = [UIImage imageNamed:@"qrLine"];
    [self addSubview:_line];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(animation1) userInfo:nil repeats:YES];
}

#pragma mark - 上下动画
-(void)animation1
{
    if (upOrdown == NO) {
        num ++;
        _line.frame = CGRectMake(0, self.frame.size.height*0.2f+2*num, _line.frame.size.width, _line.frame.size.height);
        if (2*num > self.frame.size.width-self.frame.size.width*0.4f) {
            upOrdown = YES;
        }
    }
    else {
        num --;
        _line.frame = CGRectMake(0, self.frame.size.height*0.2f+2*num, _line.frame.size.width, _line.frame.size.height);
        if (num == 2) {
            upOrdown = NO;
        }
    }
    
}


@end
