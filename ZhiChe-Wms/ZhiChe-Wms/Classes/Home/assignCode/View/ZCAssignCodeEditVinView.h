//
//  ZCAssignCodeEditVinView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZCAssignCodeEditVinViewDelegate <NSObject>

- (void)assignCodeEditVinViewScanButtonClick;

- (void)assignCodeEditVinViewCommitButtonClick;

- (void)assignCodeEditVinViewEditVin:(NSString *)vin;

@end


@interface ZCAssignCodeEditVinView : UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, weak) id<ZCAssignCodeEditVinViewDelegate> delegate;

@end
