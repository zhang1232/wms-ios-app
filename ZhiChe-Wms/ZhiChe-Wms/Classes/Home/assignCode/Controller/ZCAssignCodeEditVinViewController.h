//
//  ZCAssignCodeEditVinViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BindCodeBlock)(BOOL bind);

@interface ZCAssignCodeEditVinViewController : UIViewController

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) BindCodeBlock bindCodeBlock;

@end
