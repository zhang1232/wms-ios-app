//
//  ZCAdjustmentItemView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAdjustmentItemView.h"


@interface ZCAdjustmentItemView ()

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation ZCAdjustmentItemView
#pragma mark - 懒加载
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = ZCColor(0x000000, 0.87);
        _nameLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _nameLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.textColor = ZCColor(0x000000, 0.54);
        _contentLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.nameLabel];
        [self addSubview:self.contentLabel];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = name;
}

- (void)setContent:(NSString *)content {
    _content = content;
    self.contentLabel.text = content;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    CGSize size = [self.nameLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, space(60)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo(size.width + space(10));
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nameLabel.mas_right);
        make.right.equalTo(weakSelf.mas_right);
        make.centerY.equalTo(weakSelf.nameLabel.mas_centerY);
    }];
}

@end
