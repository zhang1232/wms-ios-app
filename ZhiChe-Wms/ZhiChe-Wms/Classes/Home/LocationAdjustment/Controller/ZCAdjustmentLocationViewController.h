//
//  ZCAdjustmentLocationViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^adjustmentLocationBlock)(NSString *location, NSString *locationId);

@interface ZCAdjustmentLocationViewController : UIViewController

@property (nonatomic, copy) adjustmentLocationBlock locationBlock;

@end
