//
//  ZCUserInfoModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCUserInfoModel.h"


@implementation ZCUserInfoPermissionModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"permissionId" : @"id"};
}

@end

@implementation ZCUserInfoStoreModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"storeId" : @"id"};
}

@end

@implementation ZCOriginLocationModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"originId" : @"id"};
}

@end

@implementation ZCUserInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"permissions" : [ZCUserInfoPermissionModel class], @"storehouses" : [ZCUserInfoStoreModel class], @"opDeliveryPoints" : [ZCOriginLocationModel class]};
}

@end

@implementation ZCAppVersionModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end
