//
//  ZCUserTokenModel.m
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCUserTokenModel.h"

@implementation ZCUserRoleModel

@end

static ZCUserTokenModel *userInfoModel = nil;
@implementation ZCUserTokenModel

+ (instancetype)sharedUserTokenModel {
    if (userInfoModel == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            userInfoModel = [[ZCUserTokenModel alloc] init];
        });
    }
    return userInfoModel;
}


+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"roles" : [ZCUserRoleModel class]};
}

@end
