//
//  ZCUserTokenModel.h
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCUserRoleModel : NSObject

@property (nonatomic, copy) NSString *authority;

@end

@interface ZCUserTokenModel : NSObject

@property (nonatomic, copy) NSString *access_token;   //用户token
@property (nonatomic, copy) NSString *token_type;
@property (nonatomic, copy) NSString *refresh_token;  //刷新token时用到的token
@property (nonatomic, copy) NSString *expires_in;     //token有效期,单位秒
@property (nonatomic, copy) NSString *scope;
@property (nonatomic, copy) NSString *gmt_create;     //token获取的时间戳
@property (nonatomic, copy) NSString *accountId;
@property (nonatomic, strong) NSArray<ZCUserRoleModel *> *roles;
@property (nonatomic, copy) NSString *tenantId;
@property (nonatomic, copy) NSString *corpName;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *jti;

+ (instancetype)sharedUserTokenModel;

@end

