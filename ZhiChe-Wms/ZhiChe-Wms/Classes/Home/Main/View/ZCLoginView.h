//
//  ZCLoginView.h
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCLoginViewDelegate <NSObject>

- (void)ZCLoginViewLoginButtonAction;

@end

typedef void(^AccountBlock)(NSString *account);
typedef void(^PasswordBlock)(NSString *password);

@interface ZCLoginView : UIView

@property (nonatomic, weak) id<ZCLoginViewDelegate> delegate;
@property (nonatomic, copy) AccountBlock accountBlock;
@property (nonatomic, copy) PasswordBlock passwordBlock;

@end
