//
//  ZCVersionChangeView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCVersionChangeView.h"

@interface ZCVersionChangeView ()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *topLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *vLineView;
@property (nonatomic, strong) UIView *hLineView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *updateButton;
@end

@implementation ZCVersionChangeView
#pragma mark - 懒加载
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = ZCColor(0xff8213, 1);
    }
    return _topView;
}

- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [[UILabel alloc] init];
        _topLabel.text = @"版本更新";
        _topLabel.textColor = [UIColor whiteColor];
        _topLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _topLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.text = @"有新版本，是否前往下载？";
        _contentLabel.textColor = ZCColor(0x000000, 0.87);
        _contentLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _contentLabel;
}

- (UIView *)vLineView {
    if (!_vLineView) {
        _vLineView = [[UIView alloc] init];
        _vLineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _vLineView;
}

- (UIView *)hLineView {
    if (!_hLineView) {
        _hLineView = [[UIView alloc] init];
        _hLineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _hLineView;
}


@end
