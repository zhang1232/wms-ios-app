//
//  ZCCancellingStockModel.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import "ZCCancellingStockModel.h"

@implementation ZCCancellingStockDetailModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"cancelId" : @"id"};
}

@end

@implementation ZCCancellingStockConditionModel

@end

@implementation ZCCancellingStockModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCCancellingStockDetailModel class]};
}
@end
