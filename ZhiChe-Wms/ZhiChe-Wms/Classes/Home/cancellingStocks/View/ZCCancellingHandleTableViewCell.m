//
//  ZCCancellingHandleTableViewCell.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import "ZCCancellingHandleTableViewCell.h"

@interface ZCCancellingHandleTableViewCell ()

@property (nonatomic, strong) UIButton *takeTaskButton;
@property (nonatomic, strong) UIView *lineView;

@end


@implementation ZCCancellingHandleTableViewCell
#pragma mark - 懒加载
- (UIButton *)takeTaskButton {
    if (!_takeTaskButton) {
        _takeTaskButton = [[UIButton alloc] init];
        [_takeTaskButton setTitle:@"领取任务" forState:UIControlStateNormal];
        [_takeTaskButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_takeTaskButton setBackgroundColor:ZCColor(0xff8213, 1)];
        [_takeTaskButton addTarget:self action:@selector(takeTask:) forControlEvents:UIControlEventTouchUpInside];
        _takeTaskButton.layer.masksToBounds = YES;
        _takeTaskButton.layer.cornerRadius = space(6);
    }
    return _takeTaskButton;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 按钮点击
- (void)takeTask:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(cancellingHandleTableViewCellTakeTaskAction:)]) {
        [self.delegate cancellingHandleTableViewCellTakeTaskAction:sender];
    }
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.takeTaskButton];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [self.takeTaskButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(60));
        make.right.equalTo(weakSelf.mas_right).offset(-space(60));
        make.top.equalTo(weakSelf.mas_top).offset(space(30));
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-space(30));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.right.equalTo(weakSelf.mas_right).offset(-space(20));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
