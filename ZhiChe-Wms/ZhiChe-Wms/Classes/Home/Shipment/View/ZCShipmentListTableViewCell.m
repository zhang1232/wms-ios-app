//
//  ZCShipmentListTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/10.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCShipmentListTableViewCell.h"


@interface ZCShipmentListTableViewCell ()

@property (nonatomic, strong) UIView *titleV;
@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UILabel *commandTitleLabel;
@property (nonatomic, strong) UILabel *commandNoLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UIView *contentV;
@property (nonatomic, strong) UIImageView *originImageV;
@property (nonatomic, strong) UIImageView *destImageV;
@property (nonatomic, strong) UIImageView *carImageV;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destLabel;
@property (nonatomic, strong) UILabel *carLabel;
@property (nonatomic, strong) UILabel *commandCountTitleLabel;
@property (nonatomic, strong) UILabel *commandCountLabel;
@property (nonatomic, strong) UILabel *loadingCountTitleLabel;
@property (nonatomic, strong) UILabel *loadingCountLabel;
@property (nonatomic, strong) UILabel *plateNoTitleLabel;
@property (nonatomic, strong) UILabel *plateNoLabel;

@end


@implementation ZCShipmentListTableViewCell
#pragma mark - 懒加载
- (UIView *)titleV {
    if (!_titleV) {
        _titleV = [[UIView alloc] init];
        _titleV.backgroundColor = [UIColor whiteColor];
    }
    return _titleV;
}

- (UIImageView *)imageV {
    if (!_imageV) {
        _imageV = [[UIImageView alloc] init];
        _imageV.image = [UIImage imageNamed:@"shipment_command"];
    }
    return _imageV;
}

- (UILabel *)commandTitleLabel {
    if (!_commandTitleLabel) {
        _commandTitleLabel = [[UILabel alloc] init];
        _commandTitleLabel.text = @"指令号";
        _commandTitleLabel.textColor = ZCColor(0x4b5c66, 1);
        _commandTitleLabel.font = [UIFont systemFontOfSize:FontSize(30)];
    }
    return _commandTitleLabel;
}

- (UILabel *)commandNoLabel {
    if (!_commandNoLabel) {
        _commandNoLabel = [[UILabel alloc] init];
        _commandNoLabel.textColor = ZCColor(0x4b5c66, 1);
        _commandNoLabel.font = [UIFont systemFontOfSize:FontSize(32)];
        _commandNoLabel.text = @"------";
    }
    return _commandNoLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.textColor = ZCColor(0xff8213, 1);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(30)];
        _statusLabel.text = @"--------";
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UIView *)contentV {
    if (!_contentV) {
        _contentV = [[UIView alloc] init];
        _contentV.backgroundColor = [UIColor whiteColor];
    }
    return _contentV;
}

- (UIImageView *)originImageV {
    if (!_originImageV) {
        _originImageV = [[UIImageView alloc] init];
        _originImageV.image = [UIImage imageNamed:@"from"];
    }
    return _originImageV;
}

- (UIImageView *)destImageV {
    if (!_destImageV) {
        _destImageV = [[UIImageView alloc] init];
        _destImageV.image = [UIImage imageNamed:@"to"];
    }
    return _destImageV;
}

- (UIImageView *)carImageV {
    if (!_carImageV) {
        _carImageV = [[UIImageView alloc] init];
        _carImageV.image = [UIImage imageNamed:@"shipment_ship"];
    }
    return _carImageV;
}

- (UILabel *)originLabel {
    if (!_originLabel) {
        _originLabel = [[UILabel alloc] init];
        _originLabel.textColor = ZCColor(0x000000, 0.87);
        _originLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _originLabel.text = @"-------";
    }
    return _originLabel;
}

- (UILabel *)destLabel {
    if (!_destLabel) {
        _destLabel = [[UILabel alloc] init];
        _destLabel.textColor = ZCColor(0x000000, 0.87);
        _destLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _destLabel.text = @"--------";
    }
    return _destLabel;
}

- (UILabel *)carLabel {
    if (!_carLabel) {
        _carLabel = [[UILabel alloc] init];
        _carLabel.textColor = ZCColor(0x000000, 0.87);
        _carLabel.font = [UIFont systemFontOfSize:FontSize(28)];
//        _carLabel.text = @"--------";
    }
    return _carLabel;
}

- (UILabel *)commandCountTitleLabel {
    if (!_commandCountTitleLabel) {
        _commandCountTitleLabel = [[UILabel alloc] init];
        _commandCountTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _commandCountTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _commandCountTitleLabel.text = @"指令数量";
    }
    return _commandCountTitleLabel;
}

- (UILabel *)commandCountLabel {
    if (!_commandCountLabel) {
        _commandCountLabel = [[UILabel alloc] init];
        _commandCountLabel.textColor = ZCColor(0x000000, 0.54);
        _commandCountLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _commandCountLabel.text = @"----";
    }
    return _commandCountLabel;
}

- (UILabel *)loadingCountTitleLabel {
    if (!_loadingCountTitleLabel) {
        _loadingCountTitleLabel = [[UILabel alloc] init];
        _loadingCountTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _loadingCountTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _loadingCountTitleLabel.text = @"交验数量";
    }
    return _loadingCountTitleLabel;
}

- (UILabel *)loadingCountLabel {
    if (!_loadingCountLabel) {
        _loadingCountLabel = [[UILabel alloc] init];
        _loadingCountLabel.textColor = ZCColor(0x000000, 0.54);
        _loadingCountLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _loadingCountLabel.text = @"-------";
    }
    return _loadingCountLabel;
}

- (UILabel *)plateNoTitleLabel {
    if (!_plateNoTitleLabel) {
        _plateNoTitleLabel = [[UILabel alloc] init];
        _plateNoTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _plateNoTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _plateNoTitleLabel.text = @"车牌号";
    }
    return _plateNoTitleLabel;
}

- (UILabel *)plateNoLabel {
    if (!_plateNoLabel) {
        _plateNoLabel = [[UILabel alloc] init];
        _plateNoLabel.textColor = ZCColor(0x000000, 0.54);
        _plateNoLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _plateNoLabel.text = @"-------";
    }
    return _plateNoLabel;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.titleV];
        [self.titleV addSubview:self.imageV];
        [self.titleV addSubview:self.commandTitleLabel];
        [self.titleV addSubview:self.commandNoLabel];
        [self.titleV addSubview:self.statusLabel];
        [self.titleV addSubview:self.lineView];
        
        [self addSubview:self.contentV];
        [self.contentV addSubview:self.originImageV];
        [self.contentV addSubview:self.originLabel];
        [self.contentV addSubview:self.commandCountTitleLabel];
        [self.contentV addSubview:self.commandCountLabel];
        [self.contentV addSubview:self.destImageV];
        [self.contentV addSubview:self.destLabel];
        [self.contentV addSubview:self.loadingCountTitleLabel];
        [self.contentV addSubview:self.loadingCountLabel];
        [self.contentV addSubview:self.carImageV];
        [self.contentV addSubview:self.carLabel];
        [self.contentV addSubview:self.plateNoTitleLabel];
        [self.contentV addSubview:self.plateNoLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setShipModel:(ZCShipmentModel *)shipModel {
    _shipModel = shipModel;
    _commandNoLabel.text = shipModel.sourceNo;
    _statusLabel.text = [shipModel.status isEqualToString:@"BS_CREATED"] ? @"已调度" : @"已发运";
    _originLabel.text = shipModel.originLocationName;
    _destLabel.text = shipModel.destLocationName;
    _carLabel.text = shipModel.serviceProviderName;
    _commandCountLabel.text = shipModel.shipCount;
    _loadingCountLabel.text = shipModel.handoverCount;
    _plateNoLabel.text = shipModel.plateNo;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(90));
    }];
    
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleV.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.titleV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(4, 12));
    }];
    
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    CGSize rect = [self.commandTitleLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    
    [_commandTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.imageV.mas_right).offset(space(14));
        make.centerY.equalTo(weakSelf.imageV.mas_centerY);
        make.width.mas_equalTo(rect.width + space(10));
    }];
    
    [_commandNoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.commandTitleLabel.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.commandTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.statusLabel.mas_left).offset(-space(20));
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.titleV.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.titleV.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleV.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.titleV.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.titleV.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    [_contentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(weakSelf.titleV.mas_bottom);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
    
    [_originImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.contentV.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.contentV.mas_top).offset(space(30));
        make.size.mas_equalTo(CGSizeMake(13, 16));
    }];
    
    [_originLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.originImageV.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.originImageV.mas_centerY);
//        make.width.mas_equalTo(space(140));
        make.right.equalTo(weakSelf.commandCountTitleLabel.mas_left).offset(-space(20));
    }];
    
    [_commandCountTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.contentV.mas_centerX).offset(space(20));
        make.centerY.equalTo(weakSelf.originLabel.mas_centerY);
    }];
    
    [_commandCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.commandCountTitleLabel.mas_right).offset(space(30));
        make.centerY.equalTo(weakSelf.commandCountTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.contentV.mas_right).offset(-space(20));
    }];
    
    [_destImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.originImageV.mas_centerX);
        make.top.equalTo(weakSelf.originImageV.mas_bottom).offset(space(30));
        make.size.mas_equalTo(CGSizeMake(13, 16));
    }];
    
    [_destLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.originLabel.mas_left);
        make.centerY.equalTo(weakSelf.destImageV.mas_centerY);
        make.right.equalTo(weakSelf.loadingCountTitleLabel.mas_left).offset(-space(20));
    }];
    
    [_loadingCountTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.commandCountTitleLabel.mas_left);
        make.centerY.equalTo(weakSelf.destLabel.mas_centerY);
    }];
    
    [_loadingCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.commandCountLabel.mas_left);
        make.centerY.equalTo(weakSelf.loadingCountTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.commandNoLabel.mas_right);
    }];
    
    [_carImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.destImageV.mas_centerX);
        make.top.equalTo(weakSelf.destImageV.mas_bottom).offset(space(30));
        make.size.mas_equalTo(CGSizeMake(13, 13));
    }];
    
    [_carLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.destLabel.mas_left);
        make.centerY.equalTo(weakSelf.carImageV.mas_centerY);
        make.right.equalTo(weakSelf.plateNoTitleLabel.mas_left).offset(-space(20));
    }];
    
    [_plateNoTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.loadingCountTitleLabel.mas_left);
        make.centerY.equalTo(weakSelf.carLabel.mas_centerY);
    }];
    
    [_plateNoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.plateNoTitleLabel.mas_right).offset(space(30));
        make.centerY.equalTo(weakSelf.plateNoTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.contentV.mas_right).offset(-space(20));
    }];
}

@end
