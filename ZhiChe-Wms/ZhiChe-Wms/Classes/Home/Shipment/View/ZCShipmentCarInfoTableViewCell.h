//
//  ZCShipmentCarInfoTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/11.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCShipmentListModel.h"

@interface ZCShipmentCarInfoTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCShipmentOrderModel *orderModel;

@end
