//
//  ZCShipmentCarInfoTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/11.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCShipmentCarInfoTableViewCell.h"

@interface ZCShipmentCarInfoTableViewCell ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *customerTitleLabel;
@property (nonatomic, strong) UILabel *customerLabel;
@property (nonatomic, strong) UILabel *vehicleTitleLabel;
@property (nonatomic, strong) UILabel *vehicleLabel;
@property (nonatomic, strong) UILabel *vinTitleLabel;
@property (nonatomic, strong) UILabel *vinLabel;
@property (nonatomic, strong) UILabel *statusTitleLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *originTitleLabel;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destTitleLabel;
@property (nonatomic, strong) UILabel *destLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation ZCShipmentCarInfoTableViewCell
#pragma mark - 懒加载
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)customerTitleLabel {
    if (!_customerTitleLabel) {
        _customerTitleLabel = [[UILabel alloc] init];
        _customerTitleLabel.text = @"客户";
        _customerTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _customerTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _customerTitleLabel;
}

- (UILabel *)customerLabel {
    if (!_customerLabel) {
        _customerLabel = [[UILabel alloc] init];
        _customerLabel.textColor = ZCColor(0x000000, 0.87);
        _customerLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _customerLabel.text = @"-------";
    }
    return _customerLabel;
}

- (UILabel *)vehicleTitleLabel {
    if (!_vehicleTitleLabel) {
        _vehicleTitleLabel = [[UILabel alloc] init];
        _vehicleTitleLabel.text = @"车型";
        _vehicleTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _vehicleTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _vehicleTitleLabel;
}

- (UILabel *)vehicleLabel {
    if (!_vehicleLabel) {
        _vehicleLabel = [[UILabel alloc] init];
        _vehicleLabel.textColor = ZCColor(0x000000, 0.87);
        _vehicleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _vehicleLabel.text = @"------";
    }
    return _vehicleLabel;
}

- (UILabel *)vinTitleLabel {
    if (!_vinTitleLabel) {
        _vinTitleLabel = [[UILabel alloc] init];
        _vinTitleLabel.text = @"VIN";
        _vinTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _vinTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _vinTitleLabel;
}

- (UILabel *)vinLabel {
    if (!_vinLabel) {
        _vinLabel = [[UILabel alloc] init];
        _vinLabel.textColor = ZCColor(0x000000, 0.87);
        _vinLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _vinLabel.text = @"-----";
    }
    return _vinLabel;
}

- (UILabel *)statusTitleLabel {
    if (!_statusTitleLabel) {
        _statusTitleLabel = [[UILabel alloc] init];
        _statusTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _statusTitleLabel.text = @"状态";
        _statusTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _statusTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.textColor = ZCColor(0xff8213, 1);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _statusLabel.text = @"-----";
    }
    return _statusLabel;
}

- (UILabel *)originTitleLabel {
    if (!_originTitleLabel) {
        _originTitleLabel = [[UILabel alloc] init];
        _originTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _originTitleLabel.text = @"起运地";
        _originTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _originTitleLabel;
}

- (UILabel *)originLabel {
    if (!_originLabel) {
        _originLabel = [[UILabel alloc] init];
        _originLabel.textColor = ZCColor(0x000000, 0.87);
        _originLabel.text = @"-----";
        _originLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _originLabel;
}

- (UILabel *)destTitleLabel {
    if (!_destTitleLabel) {
        _destTitleLabel = [[UILabel alloc] init];
        _destTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _destTitleLabel.text = @"目的地";
        _destTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _destTitleLabel;
}

- (UILabel *)destLabel {
    if (!_destLabel) {
        _destLabel = [[UILabel alloc] init];
        _destLabel.textColor = ZCColor(0x000000, 0.87);
        _destLabel.text = @"-------";
        _destLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _destLabel;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = ZCColor(0xf7f8fb, 1);
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.customerTitleLabel];
        [self.bgView addSubview:self.customerLabel];
        [self.bgView addSubview:self.vehicleTitleLabel];
        [self.bgView addSubview:self.vehicleLabel];
        [self.bgView addSubview:self.vinTitleLabel];
        [self.bgView addSubview:self.vinLabel];
        [self.bgView addSubview:self.statusTitleLabel];
        [self.bgView addSubview:self.statusLabel];
        [self.bgView addSubview:self.originTitleLabel];
        [self.bgView addSubview:self.originLabel];
        [self.bgView addSubview:self.destTitleLabel];
        [self.bgView addSubview:self.destLabel];
        [self.bgView addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setOrderModel:(ZCShipmentOrderModel *)orderModel {
    _orderModel = orderModel;
    _customerLabel.text = orderModel.customerId;
    _vehicleLabel.text = orderModel.stanVehicleType;
    _vinLabel.text = orderModel.vin;
    _statusLabel.text = [orderModel.status isEqualToString:@"BS_CREATED"] ? @"已调度" : @"已发运";
    _originLabel.text = orderModel.originLocationName;
    _destLabel.text = [NSString stringWithFormat:@"%@%@%@",orderModel.destLocationProvince,orderModel.destLocationCity,orderModel.destLocationCounty];
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.right.equalTo(weakSelf.mas_right).offset(-space(20));
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
    
    [_customerTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.bgView.mas_top).offset(space(30));
        make.left.equalTo(weakSelf.bgView.mas_left).offset(space(40));
        make.height.mas_equalTo(space(40));
    }];
    
    [_customerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.customerTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.bgView.mas_right).offset(-space(40));
        make.left.equalTo(weakSelf.customerTitleLabel.mas_right).offset(space(20));
    }];
    
    [_vehicleTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.customerTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.customerTitleLabel.mas_left);
        make.height.equalTo(weakSelf.customerTitleLabel.mas_height);
    }];
    
    [_vehicleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.vehicleTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.customerLabel.mas_right);
        make.left.equalTo(weakSelf.vehicleTitleLabel.mas_right).offset(space(20));
    }];
    
    [_vinTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.vehicleTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.vehicleTitleLabel.mas_left);
        make.height.equalTo(weakSelf.vehicleTitleLabel.mas_height);
    }];
    
    [_vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.vinTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.vehicleLabel.mas_right);
        make.left.equalTo(weakSelf.vinTitleLabel.mas_right).offset(space(20));
    }];
    
    [_statusTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.vinTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.vinTitleLabel.mas_left);
        make.height.equalTo(weakSelf.vinTitleLabel.mas_height);
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.statusTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.vinLabel.mas_right);
        make.left.equalTo(weakSelf.statusTitleLabel.mas_right).offset(space(20));
    }];
    
    [_originTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.statusTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.statusTitleLabel.mas_left);
        make.height.equalTo(weakSelf.statusTitleLabel.mas_height);
    }];
    
    [_originLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.originTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.statusLabel.mas_right);
        make.left.equalTo(weakSelf.originTitleLabel.mas_right).offset(space(20));
    }];
    
    [_destTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.originTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.originTitleLabel.mas_left);
        make.height.equalTo(weakSelf.originTitleLabel.mas_height);
    }];
    
    [_destLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.destTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.originLabel.mas_right);
        make.left.equalTo(weakSelf.destTitleLabel.mas_right).offset(space(20));
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bgView.mas_left);
        make.right.equalTo(weakSelf.bgView.mas_right);
        make.bottom.equalTo(weakSelf.bgView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
