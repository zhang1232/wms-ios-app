//
//  ZCMineChangePasswordViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineChangePasswordViewController.h"
#import "ZCMinePasswordTableViewCell.h"
#import "ZCMineNullView.h"

static NSString *passwordCellID = @"ZCMinePasswordTableViewCell";

@interface ZCMineChangePasswordViewController ()<UITableViewDelegate, UITableViewDataSource, ZCMinePasswordTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *commitButton;
@property (nonatomic, copy) NSString *pwdOld;
@property (nonatomic, copy) NSString *pwdNew;
@property (nonatomic, copy) NSString *pwdNewAgain;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ZCMineChangePasswordViewController
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, space(290) + 64) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ZCColor(0xffffff, 1);
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        [_tableView registerClass:[ZCMinePasswordTableViewCell class] forCellReuseIdentifier:passwordCellID];
        
    }
    return _tableView;
}

- (UIButton *)commitButton {
    if (!_commitButton) {
        _commitButton = [[UIButton alloc] init];
        [_commitButton setTitle:@"修改密码" forState:UIControlStateNormal];
        [_commitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commitButton setBackgroundColor:ZCColor(0xff8213, 0.6)];
        _commitButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(30)];
        _commitButton.layer.masksToBounds = YES;
        _commitButton.layer.cornerRadius = space(6);
        _commitButton.enabled = NO;
        [_commitButton addTarget:self action:@selector(commitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commitButton;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, space(30), space(30))];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftButtonToBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = @"修改密码";
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.commitButton];
    [self updateViewConstraints];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
}

#pragma mark - 按钮点击
- (void)leftButtonToBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)commitButtonClick:(UIButton *)sender {
    [self.hud showAnimated:YES];
    if ([self.pwdNew isEqualToString:self.pwdNewAgain]) {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:AccountId] forKey:@"accountId"];
        [param setObject:self.pwdNew forKey:@"password"];
        [param setObject:self.pwdOld forKey:@"oldPassword"];
        __weak typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [ZCHttpTool updateBaseURL:hostUaaURL];
            [ZCHttpTool postWithURL:UpdatePwd params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [ZCHttpTool updateBaseURL:hostURL];
                if ([responseObject[@"code"] integerValue] == 0) {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = @"修改成功";
                    [weakSelf.hud hideAnimated:YES afterDelay:SuccessTime];
                    [self.navigationController popViewControllerAnimated:YES];
                }else {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = responseObject[@"message"];
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = error.localizedDescription;
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }];
        });
        
    }else {
        self.hud.mode = MBProgressHUDModeText;
        self.hud.label.text = @"两次输入的密码不一致";
        [self.hud hideAnimated:YES afterDelay:HudTime];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCMinePasswordTableViewCell *pwdCell = [tableView dequeueReusableCellWithIdentifier:passwordCellID forIndexPath:indexPath];
    pwdCell.selectionStyle = UITableViewCellSelectionStyleNone;
    pwdCell.index = indexPath.row;
    pwdCell.delegate = self;
    if (indexPath.row == 0) {
        pwdCell.placeholder = @"请输入旧密码";
    }else if (indexPath.row == 1) {
        pwdCell.placeholder = @"请输入新密码";
    }else if (indexPath.row == 2) {
        pwdCell.placeholder = @"请再次输入新密码";
    }
    return pwdCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return space(90);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return space(20);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[ZCMineNullView alloc] init];
}


#pragma mark - ZCMinePasswordTableViewCellDelegate
- (void)minePasswordTableViewCellChangePassword:(NSString *)pwd index:(NSInteger)index {
    if (index == 0) {
        self.pwdOld = pwd;
    }else if (index == 1) {
        self.pwdNew = pwd;
    }else if (index == 2) {
        self.pwdNewAgain = pwd;
    }
    if (self.pwdOld.length > 0 && self.pwdNew.length > 0 && self.pwdNewAgain.length > 0) {
        self.commitButton.enabled = YES;
        self.commitButton.backgroundColor = ZCColor(0xff8213, 1);
    }else {
        self.commitButton.enabled = NO;
        self.commitButton.backgroundColor = ZCColor(0xff8213, 0.6);
    }
}

#pragma mark - 布局
- (void)updateViewConstraints {
    [super updateViewConstraints];
    __weak typeof(self)weakSelf = self;
    [_commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.tableView.mas_bottom).offset(space(200));
        make.left.equalTo(weakSelf.view.mas_left).offset(space(100));
        make.right.equalTo(weakSelf.view.mas_right).offset(-space(100));
        make.height.mas_equalTo(space(80));
    }];
}

@end
