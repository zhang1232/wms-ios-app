//
//  ZCMinePasswordTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZCMinePasswordTableViewCellDelegate <NSObject>

- (void)minePasswordTableViewCellChangePassword:(NSString *)pwd index:(NSInteger)index;

@end

@interface ZCMinePasswordTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, weak) id<ZCMinePasswordTableViewCellDelegate> delegate;

@end
