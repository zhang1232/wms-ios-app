//
//  ZCMineTableHeaderView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZCMineTableHeaderViewDelegate <NSObject>

- (void)mineTableHeaderViewTapWithIndex:(NSInteger)index;

@end

@interface ZCMineTableHeaderView : UIView
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) BOOL isLine;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, weak) id<ZCMineTableHeaderViewDelegate> delegate;
@end
