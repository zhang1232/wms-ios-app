//
//  ZCMineNullView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineNullView.h"

@implementation ZCMineNullView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = ZCColor(0xf5f9fd, 1);
    }
    return self;
}

@end
