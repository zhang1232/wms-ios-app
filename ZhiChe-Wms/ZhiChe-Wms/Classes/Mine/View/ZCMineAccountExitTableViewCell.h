//
//  ZCMineAccountExitTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZCMineAccountExitTableViewCellDelegate <NSObject>

- (void)mineAccountExitTableViewCellExitButton:(UIButton *)button;

@end

@interface ZCMineAccountExitTableViewCell : UITableViewCell

@property (nonatomic, weak) id<ZCMineAccountExitTableViewCellDelegate> delegate;

@end
