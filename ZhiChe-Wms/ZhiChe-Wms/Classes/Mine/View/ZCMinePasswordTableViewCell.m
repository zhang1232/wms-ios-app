//
//  ZCMinePasswordTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMinePasswordTableViewCell.h"

@interface ZCMinePasswordTableViewCell ()

@property (nonatomic, strong) UIImageView *pwdImageView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *line;

@end

@implementation ZCMinePasswordTableViewCell
#pragma mark - 懒加载
- (UIImageView *)pwdImageView {
    if (!_pwdImageView) {
        _pwdImageView = [[UIImageView alloc] init];
        _pwdImageView.image = [UIImage imageNamed:@"register_password"];
    }
    return _pwdImageView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.textColor = ZCColor(0x000000, 0.87);
        _textField.font = [UIFont systemFontOfSize:FontSize(28)];
        _textField.secureTextEntry = YES;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        [_textField addTarget:self action:@selector(changePassword:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _line;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.pwdImageView];
        [self addSubview:self.textField];
        [self addSubview:self.line];
    }
    return self;
}

#pragma mark - textfield事件
- (void)changePassword:(UITextField *)textfield {
    if ([self.delegate respondsToSelector:@selector(minePasswordTableViewCellChangePassword:index:)]) {
        [self.delegate minePasswordTableViewCellChangePassword:textfield.text index:self.index];
    }
}

#pragma mark - 属性方法
- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.textField.placeholder = placeholder;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_pwdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(space(31), space(37)));
    }];
    
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.pwdImageView.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo(space(600));
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
