//
//  ZCHttpTool.h
//  kangbs
//
//  Created by 高睿婕 on 2018/8/12.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^httpSuccessBlock)(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject);
typedef void (^httpFailureBlock)(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error);

@interface ZCHttpTool : NSObject

//设置基础URL
+ (void)updateBaseURL:(NSString *)url;

//获取token
+ (void)getRefreshTokenWithURL:(NSString *)url params:(NSDictionary *)params success:(httpSuccessBlock)success failure:(httpFailureBlock)failure;

//get 请求
+ (void)getWithURL:(NSString *_Nonnull)url params:(NSDictionary *_Nullable)params success:(httpSuccessBlock _Nullable)success failure:(httpFailureBlock _Nullable)failure;


//post 请求
+ (void)postWithURL:(NSString *_Nonnull)url params:(NSDictionary *_Nullable)params success:(httpSuccessBlock _Nullable)success failure:(httpFailureBlock _Nullable)failure;

@end
