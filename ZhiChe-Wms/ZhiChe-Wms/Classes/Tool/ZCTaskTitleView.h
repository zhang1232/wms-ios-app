//
//  ZCTaskTitleView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCTaskTitleViewDelegate <NSObject>

- (void)taskTitleViewButttonWithTitle:(NSString *)title;

@end

@interface ZCTaskTitleView : UIView

@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, weak) id<ZCTaskTitleViewDelegate> delegate;

@end
